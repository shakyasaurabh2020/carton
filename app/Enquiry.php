<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enquiry extends Model
{
    protected $fillable = [
            'user_id',
            'product_id',
            'message',
    ];

    public function user(){
        return $this->belongsTo('App\User', 'user_id','id');
    }

    public function product(){
        return $this->belongsTo('App\Product', 'product_id','id');
    }
}
