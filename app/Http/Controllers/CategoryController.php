<?php

namespace App\Http\Controllers;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Category;
use Session;
use File;
class CategoryController extends Controller
{
  //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++    list     +++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
    public function index(Request $request )
    {

        $sort = isset($request->sort) ? $request->sort : 'id';
        $direction = isset($request->direction) ? $request->direction : 'desc';
        $keyword = isset($request->search) ? $request->search : '';
        $show_page = isset($request->limit) ? $request->limit : 5;
        $query = Category::select('id', 'name','image','parent_id')
                ->orderBy($sort,  $direction );
        if (!empty($keyword)) {
            $query->where(function ($q) use ($keyword) {
                $q->where('name', 'LIKE', '%' . $keyword . '%');
            });
        }
        $lists = $query->paginate($show_page);
        $lists->appends(['search' => $keyword]);
        return view('category.category')->with(compact('lists', 'keyword', 'show_page'));
    }


//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++    create       +++++++++++++++++++++++++++++++++++++++++++++++++++++++
    public function create()
    {
        $data = null;
        $category = Category::select('id','name','parent_id')
                ->where('parent_id', null)
                ->get();
        return view('category.create-edit')->with(compact('data','category'));
    }


 //  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++      store         ++++++++++++++++++++++++++++++++++++++++++++++++++
    public function store(Request $request )
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1024'
           ]);
        try {
          if ($request->hasFile('photo')) {
                $file = $request->file('photo');
                $image_new_name = rand() . '.' . $file->getClientOriginalExtension();
                $folder_name = '/upload_images/category_images/';
                $destinationPath = public_path() . '/upload_images/category_images';
                if (!file_exists('upload_images/category_images')) {
                    mkdir('upload_images/category_images', 0777, true);
                }
                $upload = $file->move($destinationPath, $image_new_name);
                $request->request->add(['image' => $folder_name.$image_new_name]); 
            }
            $input = $request->except(['photo']);
            $store = Category::create($input);
           if($store){  
                // Session::flash('message', 'Record successfully added!');
                // Session::flash('alert-class', 'alert-success');
                // return redirect()->back();
                return back()->with('success','Record created successfully!'); 
           } else {
               return redirect()->back()->with('error', 'Oops! something went wrong!');
             }
        } catch (\Exception $e) {
             return redirect()->back()->with('error',  $e->getMessage());
        } catch (\Throwable $e) {
             return redirect()->back()->with('error',  $e->getMessage());
        }
    }



//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++     show          +++++++++++++++++++++++++++++++++++++++++++++++++++++
    public function show($id)
    {
        $data = Category::find($id);
        $category = Category::select('id','name','parent_id')
                ->where('parent_id', null)
                ->get();
        if($data){
            return view('category.create-edit')->with(compact('data','category'));
        } else {
             return redirect()->back()->with('error', 'No record found!');
        }
    }



 //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++      update          +++++++++++++++++++++++++++++++++++++++++++++++++++++
  
    public function update(Request $request, $id)
    {
        $data = Category::find($id);
        if($data){
            $request->validate([
                'type' => 'required',
                'name' => 'required',
                'description' => 'required',
                'parent_id' => 'required_if:type,1',
                'photo' => Rule::requiredIf( function () use ( $data){
                    return  $data->image == null;
                     }),
               ]);
            try {
              if ($request->hasFile('photo')) {
                    $file = $request->file('photo');
                    $image_new_name = rand() . '.' . $file->getClientOriginalExtension();
                    $folder_name = '/upload_images/category_images/';
                    $destinationPath = public_path() . '/upload_images/category_images';
                    if (!file_exists('upload_images/category_images')) {
                        mkdir('upload_images/category_images', 0777, true);
                    }
                    $upload = $file->move($destinationPath, $image_new_name);
                    $request->request->add(['image' =>   $folder_name.$image_new_name]); 

                    $previous_image = public_path("{$data->image}"); // get previous image from folder
                    if (File::exists($previous_image)) {      // unlink or remove previous image from folder
                        unlink($previous_image);
                    }
                  
                }
                $input = $request->except(['photo']);
                $update =  $data->update($input);
               if($update){  
                    return back()->with('success','Record updated successfully!'); 
               } else {
                   return redirect()->back()->with('error', 'Oops! something went wrong!');
                 }
            } catch (\Exception $e) {
                 return redirect()->back()->with('error',  $e->getMessage());
            } catch (\Throwable $e) {
                 return redirect()->back()->with('error',  $e->getMessage());
            }
        } else {
             return redirect()->back()->with('error', 'No record found!');
        }
    }



 //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++       delete         +++++++++++++++++++++++++++++++++++++++++++++++++++++
    public function destroy($id)
    {
        $find = Category::find($id);
        if ($find) {
            unlink(public_path() . $find->image);
            $find ->delete();
            return redirect()->back()->with('success','Record deleted successfully!');
        } else {   
            return redirect()->back()->with('error','No record found!');
        }
    }

}
