<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use Carbon\Carbon;

class ProductController extends Controller
{
    public function index(Request $request){
        $title = isset($request->title) ? $request->title :'';
        $show_page = isset($request->limit) ? $request->limit : 20;
        $query = Product::with('user','category')
                       ->select('id','user_id','category_id','name','pattern','color','image','min_price','max_price','discount','created_at','feature')
                       ->orderBy('id','DESC')
                       ->where('status', 1);

            if($title == 'Latest Products'){
                $query->where(function ($q) {
                    $q->whereDate('created_at', '>', Carbon::now()->subDays(30));
                });
            }
            if($title == 'Featured Products'){
                $query->where(function ($q) {
                   $q->where('feature', 1);
              });
            } 
            if($title == 'undefined'){
                $title = 'All Products';
            }
        $lists = $query->paginate($show_page);
        $count_product = Product::where('status', 1)->count();
        return response()->json([
                'product' =>  $lists,
                'count_product' =>  $count_product,
                'title' => $title,
                'status' => 'success'
           ],200);
       }

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++   show one record  +++++++++++++++++++++++++++++++++++++++++++++++++++++++
   public function show(Request $request){
    $data = Product::with('user','category','product_images')->where('status', 1)->find($request->id);
    if($data){
        return response()->json([
            'product' =>   $data,
            'status' => 'success'
     ],200);
    } else {
        return response()->json([
            'status' => 'error',
            'message' => 'Record not found !'
      ],400);
    }       
  }
}
