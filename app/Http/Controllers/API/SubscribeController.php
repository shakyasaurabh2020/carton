<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Subscribe;

class SubscribeController extends Controller
{
   
    public function store(Request $request)
    {

        $request->validate([
            'email' => 'required|email|unique:subscribies,email', 
           ]);

           $store = Subscribe::create($request->all());
           if ($store) { 
            return response([
                'status' => 'success',
                'msg' => 'Record save successfully'
              ], 200);    
          } else {   
            return response([
                'status' => 'error', 
                'msg' => 'Oops! something went wrong'
              ], 403);
         }     
     }

  
}
