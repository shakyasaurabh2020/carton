<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use App\Product;
use App\Blog;
use Carbon\Carbon;
use App\Enquiry;

class HomeController extends Controller
{
    public function index(){
        $category = Category::orderBy('id','DESC')
            ->get();

        $feature_product = Product::with('user','category')
            ->select('id','user_id','category_id','name','pattern','color','image','min_price','max_price','discount')
            ->orderBy('id','DESC')
            ->where('feature', 1)
            ->where('status', 1)
            ->take(4)
            ->get();
        $count_feature_product = Product::where('feature', 1)->count();

        $latest_product = Product::with('user','category')
            ->orderBy('id','DESC')
            ->where('status', 1)
            ->whereDate('created_at', '>', Carbon::now()->subDays(30))
            ->take(4)
            ->get();
        $count_latest_product  = Product::whereDate('created_at', '>', Carbon::now()->subDays(30))
                 ->count();

        $latest_blog = Blog::orderBy('id','DESC')
            ->where('status', 1)
            ->whereDate('created_at', '>', Carbon::now()->subDays(30))
            ->take(3)
            ->get();
        $count_latest_blog = Blog::where('status', 1)
            ->whereDate('created_at', '>', Carbon::now()->subDays(30))
            ->count();
       $total_product = Product::count();
         return response()->json([
                'category' =>  $category,
                'feature_product' =>  $feature_product,
                'count_feature_product' =>  $count_feature_product,
                'latest_product' =>  $latest_product,
                'count_latest_product' =>  $count_latest_product,
                'latest_blog' =>  $latest_blog,
                'count_latest_blog' =>  $count_latest_blog,
                'total_product' => $total_product,
                'status'=>'success'
            ],200);
     }

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++   inquiry  +++++++++++++++++++++++++++++++++++++++++++++++++++
     public function inquiry(Request $request){
       $create = Enquiry::create($request->all());
       if($create){
            return response()->json([
                'status' => 'success'
            ],200);   
        } else {
            return response()->json([
                'status' => 'error'
            ],403); 
        }          
      }
//   +++++++++++++++++++++++++++++++++++++++++++++++++++++++ end   +++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
}
