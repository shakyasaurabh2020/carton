<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    public function index(Request $request){
        $show_page = isset($request->limit) ? $request->limit : 20;
        $query = Category::select('id','name','image')
                       ->orderBy('id','DESC');
        $lists = $query->paginate($show_page);
       return response()->json([
               'category' =>  $lists,
               'status' => 'success'
           ],200);
       }
}
