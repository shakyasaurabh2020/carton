<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\ProductImage;
use DB;

class SellerProductController extends Controller
{
 //  +++++++++++++++++++++++++++++++++++++++++++++++++  list  +++++++++++++++++++++++++++++++++++++++++++++++++++++++
    public function index(Request $request){
        $id = $request->id;
        $keyword = isset($request->search) ? $request->search : '';
        $show_page = isset($request->limit) ? $request->limit : 15;
        $query = Product::with('category','user')
               ->select('id', 'name','image','user_id','category_id','min_price','max_price','status','feature')
               ->where('user_id', $id)
               ->orderBy('id', 'desc');
        if (!empty($keyword)) {
            $query->where(function ($q) use ($keyword) {
                $q->where('name', 'LIKE', '%' . $keyword . '%');
            });
        }
        $lists = $query->paginate($show_page);
        $lists->appends(['search' => $keyword]);
      if($lists){
            return response()->json([
                'product' =>  $lists,
                'status' => 'success'
            ],200);
      } else {
        return response()->json([
            'status' => 'success'
        ],403);  
      }    
    }

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++    show     +++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
    public function show(Request $request)
      {
        $id= $request->id;
        $data = Product::with('product_images')->find($id);
        if($data){
                return response()->json([
                    'data' =>  $data,
                    'status' => 'success'
                ],200);
        } else {
            return response()->json([
                'status' => 'success'
            ],403);  
        }   
    }

    //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++    dropdown     +++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
    public function dropdown()
      {
        $category = Category::select('id', 'name')->get();
        $dimension_unit = Product::$dimension_unit;
            return response()->json([
                'category' =>  $category,
                'dimension_unit' =>  $dimension_unit,
                'status' => 'success'
            ],200);  
    }

//  +++++++++++++++++++++++++++++++++++++++++++++++++  delete +++++++++++++++++++++++++++++++++++++++++++++++++++++++
    public function delete(Request $request){
        $product = Product::find($request->id);
        if($product){
            $product->delete();
            return response([
                'status' => 'success',
                'msg' => 'Record deleted successfully'
            ], 200);
        } else {
            return response()->json([
                'status' => 'error', 
                'msg' => 'Record not found'
            ], 403);
        }
    }

  //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++    store     +++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
    public function store(Request $request)
      {
        //   dd($request->all());
        // $request->validate([
        //     'category_id' => 'required|numeric',
        //     'name' => 'required',
        //     'color' => 'required',
        //     'min_price' => 'required|numeric',
        //     'max_price' => 'required|numeric',
        //     'pattern' => 'required',
        //     'sku' => 'required',
        //     'description' => 'required',
        //     'unit' => 'required',
        //     'weight' => 'required|numeric',
        //     'photo' => 'required|image|mimes:jpeg,png,jpg,svg|max:1024',
        //     'gallery_images' => 'required|array|between:1,7',
        //     'gallery_images.*' => 'image|mimes:jpg,jpeg,png,svg|max:20000',
        //    ]);
        //    $user_id = Auth::id();
           DB::beginTransaction();
        try {
            $folder_name = '/upload_images/product_images/';
            if ($request->avatar) {
                    $image_parts = explode(";base64,", $request->avatar);
                    $image_type_aux = explode("image/", $image_parts[0]);
                    $image_type = isset($image_type_aux[1]) ? $image_type_aux[1] : '';
                    $image_base64 = isset($image_parts[1]) ? base64_decode($image_parts[1]) : '';
                    if ($image_base64) {
                         $fileName = rand() . '.' . $image_type;
                         $destinationPath = public_path() . $folder_name;
                         $file =  $destinationPath . $fileName;
                         if (!file_exists('upload_images/product_images')) {
                                mkdir('upload_images/product_images', 0777, true);
                            }
                         $upload = file_put_contents($file, $image_base64);
                         $request->request->add(['image' =>  $folder_name.$fileName]); 
                    }
             }

            $input = $request->except(['photo','gallery_images','avatar']);
            $store = Product::create($input);
           if($store){ 
               $product_id = $store->id;
               if ($request->product_img) {
                foreach ($request->product_img as $key => $image) {                           
                      $new_image = $image;
                      $image_parts = explode(";base64,", $new_image);
                      $image_type_aux = explode("image/", $image_parts[0]);
                      $image_type = isset($image_type_aux[1]) ? $image_type_aux[1] : '';
                      $image_base64 = isset($image_parts[1]) ? base64_decode($image_parts[1]) : '';
                      if ($image_base64) {
                          $fileName = rand() . '.' . $image_type;
                          $destinationPath = public_path() . $folder_name;
                          $file =  $destinationPath . $fileName;
                          if (!file_exists('upload_images/product_images')) {
                                  mkdir('upload_images/product_images', 0777, true);
                              }
                          $upload = file_put_contents($file, $image_base64);
                          $store = ProductImage::create([
                              'product_id' => $product_id,
                              'image' => $folder_name.$fileName,
                            ]);  
                      }
                  }
              }
                DB::commit();            
                return response([
                    'status' => 'success',
                    'msg' => 'Record created successfully'
                  ], 200);
             }
        } catch (\Exception $e) {
            DB::rollback();
            return response([
                'status' => 'error',
                'msg' => $e->getMessage()
              ], 403);

        } catch (\Throwable $e) {
            DB::rollback();
            return response([
                'status' => 'error',
                'msg' => $e->getMessage()
              ], 403);
        }
    }


    
  //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++    update     +++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
  public function update(Request $request,  $id)
  {
      $data = Product::find($id);
      if($data){
          $request->validate([
              'category_id' => 'required|numeric',
              'name' => 'required',
              'color' => 'required',
              'min_price' => 'required|numeric',
              'max_price' => 'required|numeric',
            //   'discount' => 'required|numeric',
              'pattern' => 'required',
              'sku' => 'required',
              'description' => 'required',
            //   'length' => 'required|numeric',
            //   'width' => 'required|numeric',
            //   'height' => 'required|numeric',
              'unit' => 'required',
              'weight' => 'required|numeric',
             ]);
             DB::beginTransaction();
          try {
               $folder_name = '/upload_images/product_images/';
                if ($request->avatar) {
                    $image_parts = explode(";base64,", $request->avatar);
                    $image_type_aux = explode("image/", $image_parts[0]);
                    $image_type = isset($image_type_aux[1]) ? $image_type_aux[1] : '';
                    $image_base64 = isset($image_parts[1]) ? base64_decode($image_parts[1]) : '';
                    if ($image_base64) {
                        $fileName = rand() . '.' . $image_type;
                        $destinationPath = public_path() . $folder_name;
                        $file =  $destinationPath . $fileName;
                        if (!file_exists('upload_images/product_images')) {
                                mkdir('upload_images/product_images', 0777, true);
                            }
                        $upload = file_put_contents($file, $image_base64);
                        $request->request->add(['image' =>  $folder_name.$fileName]); 
                    }
                }
              $input = $request->except(['photo','gallery_images','avatar']);
              $update =  $data->update($input);
             if($update){ 
              if ($request->product_img) {
                      foreach ($request->product_img as $key => $image) {                           
                            $new_image = $image;
                            $image_parts = explode(";base64,", $new_image);
                            $image_type_aux = explode("image/", $image_parts[0]);
                            $image_type = isset($image_type_aux[1]) ? $image_type_aux[1] : '';
                            $image_base64 = isset($image_parts[1]) ? base64_decode($image_parts[1]) : '';
                            if ($image_base64) {
                                $fileName = rand() . '.' . $image_type;
                                $destinationPath = public_path() . $folder_name;
                                $file =  $destinationPath . $fileName;
                                if (!file_exists('upload_images/product_images')) {
                                        mkdir('upload_images/product_images', 0777, true);
                                    }
                                $upload = file_put_contents($file, $image_base64);
                                $store = ProductImage::create([
                                    'product_id' => $id,
                                    'image' => $folder_name.$fileName,
                                  ]);  
                            }
                        }
                    }
                  DB::commit(); 
                  return response([
                    'status' => 'success',
                    'msg' => 'Record deleted successfully'
                  ], 200);
               }
          } catch (\Exception $e) {
              DB::rollback();
              return response([
                'status' => 'error', 
                'msg' => $e->getMessage()
              ], 403);
          } catch (\Throwable $e) {
              DB::rollback();
              return response([
                'status' => 'error', 
                'msg' => $e->getMessage()
              ], 403);
          }
      } else {
        return response([
            'status' => 'error', 
            'msg' =>' No record found'
          ], 403);
      }
  }
      
//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++    destroy_product_gallery     +++++++++++++++++++++++++++++++++++++++++++++++++++++++ 

    public function destroy_product_gallery(Request $request)
    {
        $find = ProductImage::find($request->id);
        if ($find) {
            unlink(public_path() . $find->image);
            $find->delete();   
            return response([
                'status' => 'success',
                'msg' => 'Record deleted successfully'
              ], 200);    
        } else {   
            return response([
                'status' => 'error', 
                'msg' => 'No record found!'
              ], 403);
        }
}



// ==================   status toggle button ======================
public function status(Request $request)
{
    // dd($request->all());
    $id = $request->id;
    $result = Product::find($id);
    if ($result) {
        $update_status = Product::where('id', $id)->update([
            'status' => $request->status,
        ]);
        if ($update_status) {           
            return response()->json([
                'message' => 'status updated successfully!',
                'status' => 'success',
            ],200);
        } else {  
            return response()->json([
                'message' => 'Oops! something went wrong!',
                'status' => 'error',
            ],403);
        }
    } else {
        return response()->json([
            'message' => 'No record found!',
            'status' => 'error',
        ],403);
    }
}


// ==================    toggle button  for product feature ======================
public function feature(Request $request)
{
    $id = $request->id;
    $result = Product::find($id);
    if ($result) {
        $update_status = Product::where('id', $id)->update([
            'feature' => $request->feature,
        ]);
        if ($update_status) {           
            return response()->json([
                'message' => 'status updated successfully!',
                'status' => 'success',
            ],200);
        } else {  
            return response()->json([
                'message' => 'Oops! something went wrong!',
                'status' => 'error',
            ],403);
        }
    } else {
        return response()->json([
            'message' => 'No record found!',
            'status' => 'error',
        ],403);
    }
}
// -----------------  end ------------------------
}
