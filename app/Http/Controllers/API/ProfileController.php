<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\User;
use App\UserMeta;

class ProfileController extends Controller
{
    public function profile_info(Request $request){
        $user = User::with('user_meta')->where('id', $request->id)->first();
        if($user){
            return response()->json([
                'user' =>  $user,
                'status' => 'success'
            ],200);
        } else {
            return response()->json([
                'status' => 'error',
                'msg' => 'Record not found'
            ],400);
        }   
    }
    
    public function contact_info(Request $request){
         $id = $request->id;
         $user = User::where('id', $id)->first();
        if($user){
             $input = $request->except(['id']);
             $update = $user-> update($input);
            if($update){
                return response()->json([
                    'status' => 'success',
                    'msg' => 'Record updated successfully'
                ],200);
             } else {
                    return response()->json([
                        'status' => 'error',
                        'msg' => 'Oops! something went wrong'
                    ],400);
                }   
          }  else {
            return response()->json([
                'status' => 'error',
                'msg' => 'Record not found'
            ],400); 
          }         
    }

    public function company_info(Request $request){
        $user_id = $request->id;
        $user_meta = UserMeta::where('user_id', $user_id)->first();
       if($user_meta){
           $input = $request->all();
           $update = $user_meta->update($input);
           if($update){
               return response()->json([
                   'status' => 'success',
                   'msg' => 'Record updated successfully'
               ],200);
            } else {
                   return response()->json([
                       'status' => 'error',
                       'msg' => 'Oops! something went wrong'
                   ],400);
               }   
         }  else {
           return response()->json([
               'status' => 'error',
               'msg' => 'Record not found'
           ],400); 
         }             
    }

    
    public function password_change(Request $request){
        $id = $request->id;
        $user = User::where('id', $id)->first();
       if($user){
                //   dd($request->all());
        if (!(Hash::check($request->get('password'), $user->password))) {
            // The passwords matches
            return response()->json([
                'msg' => "Your current password does not matches with the password you provided. Please try again."
            ], 403);
         }
        if(strcmp($request->get('password'), $request->get('new_password')) == 0){
            //Current password and new password are same
            return response()->json([
                'msg' => "New Password cannot be same as your current password. Please choose a different password."
            ], 403);
        }
        // $validatedData = $request->validate([
        //     // 'current_password' => 'required',
        //     'new_password' => 'required|min:8',
        // ]);
        //Change Password 
            $user->password = bcrypt($request->get('new_password'));
            $user = $user->save();
     if($user){
        return response()->json([
            'status' => 'success',
            'msg' => "Password changed successfully !"
        ],200);
      }  else {
        return response()->json([
            'status' => 'error',
            'msg' => "Oops! password not updated !"
        ],403);
       }
         }  else {
           return response()->json([
               'status' => 'error',
               'msg' => 'Record not found'
           ],400); 
         }               
    }
  }
