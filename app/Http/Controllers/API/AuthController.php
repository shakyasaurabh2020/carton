<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Country;
use App\State;
use App\UserMeta;
use App\PasswordReset;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use Illuminate\Support\Facades\Hash;
use DB;
use App\Notifications\PasswordResetRequest;
use Carbon\Carbon;

class AuthController extends Controller
{
    private $request;

    public function __construct(Request $request) {
        $this->request = $request;
    }

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++  new registration   +++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
    public function registration(Request $request){
        $validator = $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required', 
            'psw_repeat' => 'required|same:password',    
            'mobile' => 'required',
            'role_id' => 'required',  
           ]);
           DB::beginTransaction();
           try {
              if(isset($request->password)){
                  $password =  bcrypt($request->password);
                  $request->request->add(['password' => $password]); 
               }
              $input = $request->all();
              $store = User::create($input);
              if($store){
                $user_meta = new UserMeta;
                $store_usermeta = $user_meta->create([
                     'user_id' => $store['id'],
                     'company_name' => $request->company_name,
                 ]);
                if($store_usermeta){    
                    DB::commit(); 
                    return response()->json([
                        'status' => 'success', 
                        'msg' => 'Record created successfully!'
                    ], 200);
                } else {
                    return response()->json([
                        'status' => 'error',
                         'msg' => 'something went wrong'
                        ], 403);  
                 }
               }
            
              } catch (\Exception $e) {
                DB::rollback();
                 return response()->json([
                     'status' => 'error',
                     'msg' => $e->getMessage()
                    ], 403);  
              } catch (\Throwable $e) {
                DB::rollback();
                return response()->json([
                    'status' => 'error',
                    'msg' => $e->getMessage()
                   ], 403);
              }
      }


//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++  jwt   +++++++++++++++++++++++++++++++++++++++++++++++++++++++ 

  protected function jwt(User $user) {
        $payload = [
            'iss' => "lumen-jwt", // Issuer of the token
            'sub' => $user->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued. 
            'exp' => time() + 60*60 // Expiration time
        ];       
        // As you can see we are passing `JWT_SECRET` as the second parameter that will 
        // be used to decode the token in the future.
        return JWT::encode($payload, env('JWT_SECRET'));
    } 

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++  login   +++++++++++++++++++++++++++++++++++++++++++++++++++++++ 

    public function login(User $user) {
        $this->validate($this->request, [
            'email'     => 'required|email',
            'password'  => 'required'
        ]);
        // Find the user by email
        $user = User::with('user_meta','country')->where('email', $this->request->input('email'))->first();
        if (!$user) {
            return response()->json([
                'status'=> 'error',
                'error' => 'Invalid Email'
            ], 400);
        }
        // Verify the password and generate the token
        // echo Hash::make('123456');    die('here');
        if (Hash::check($this->request->input('password'), $user->password)) {
            return response()->json([
                'data'=>  $user ,
                'token' => $this->jwt($user),
                'status'=> 'success',
            ], 200);
        }
        // Bad Request response
        return response()->json([
            'status'=> 'error',
            'error' => 'Invalid Email or Password'
        ], 400);
    }

    //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++  country  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
    public function country(){
     $country_list = Country::get();
        if($country_list){
            return response()->json([
                'country_list'=>  $country_list ,
                'status'=> 'success',
            ], 200);
        } else {
            return response()->json([
                'status'=> 'error',
                'message' => 'Record not fount!'
            ], 403);
        }
    }

    //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++  State  +++++++++++++++++++++++++++++++++++++++++++++++++++++++    
     public function state(Request $request){
        $state_list = State::where('country_id',$request->country_id)->get();
        if($state_list){
           return response()->json([
               'state_list'=>  $state_list ,
               'status'=> 'success',
           ], 200);
        } else {
           return response()->json([
               'status'=> 'error',
               'message' => 'Record not fount!'
           ], 403);
         }        
      }

 //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++  State  +++++++++++++++++++++++++++++++++++++++++++++++++++++++    
      public function forgot_password(Request $request){
          $user = User::where('email', $request->email)->first();
          if (!$user)
              return response()->json([
                  'status'=> 'error',
                  'message' => "We can't find a user with that e-mail address."
              ], 403);
          $passwordReset = PasswordReset::updateOrCreate([
                  'email' => $user->email,
                  'token' => str_random(60)
               ]);
          if ($user && $passwordReset){
              $user->notify(
                  new PasswordResetRequest($passwordReset)
              );
           }
          return response()->json([
               'data' => $user,
               'status' => 'success',
               'message' => 'We have e-mailed your password reset link!'
          ]);    
    }

 //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++  match_token forget password  +++++++++++++++++++++++++++++++++++++++++++++++++++++++   
    public function match_token($token)
    {
        $passwordReset = PasswordReset::where('token', $token)->first();
        if (!$passwordReset)
            return response()->json([
                'message' => 'This password reset token is invalid.'
            ], 403);
        if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
            $passwordReset->delete();
            return response()->json([
                'message' => 'This password reset token is invalid.'
            ], 403);
        }
        return response()->json([
            'data' => $passwordReset,
            'status' => 'success',
            'message' => 'This password reset token is valid.'
            ]);
    }

 //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++  match_token forget password  +++++++++++++++++++++++++++++++++++++
    public function reset_password(Request $request)
    {
        // dd( $request->all());
        // $request->validate([
        //     'password' => 'required',
        //     'token' => 'required'
        // ]);

        $passwordReset = PasswordReset::where('token', $request->token)->first();
        if (!$passwordReset)
            return response()->json([
                'message' => 'This password reset token is invalid.'
            ], 403);
        $user = User::where('email', $passwordReset->email)->first();
        if (!$user)
            return response()->json([
                'message' => "We can't find a user with that e-mail address."
            ], 403);
        $user->password = bcrypt($request->password);
        $user->save();
         $passwordReset->where('token', $request->token)->delete();
        return response()->json([
            'data' => $user,
            'status' => 'success',
            'message' => 'password updated successfully!'
        ],200);
    }
//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++  end   +++++++++++++++++++++++++++++++++++++++++++++++++++++++ 

}
