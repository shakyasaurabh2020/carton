<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Enquiry;

class InquiryController extends Controller
{   
    public function index(Request $request){
        $show_page = isset($request->limit) ? $request->limit : 20;
        $query = Enquiry::with('user','product')->select('id','user_id','product_id','message')
                       ->where('user_id',$request->id)
                       ->orderBy('id','DESC');
        $lists = $query->paginate($show_page);
       return response()->json([
               'data' =>  $lists,
               'status' => 'success'
           ],200);
       }

    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++   show one record  +++++++++++++++++++++++++++++++++++++++++++++++++++++++
    public function show(Request $request){
        $data = Enquiry::with('user','product')->find($request->id);
        if($data){
            return response()->json([
                'data' =>   $data,
                'status' => 'success'
        ],200);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Record not found !'
        ],400);
      }  
    }

   // +++++++++++++++++++++++++++++++++++++++++++++++++++++++ end ++++++++++++++++++++++++++
}
