<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
class SearchController extends Controller
{
    public function index(Request $request){
     $keyword = isset($request->search) ? $request->search : '';
     $type = isset($request->type) ? $request->type : '';
     $show_page = isset($request->limit) ? $request->limit : 20;
     $query = Product::with('user','category')
                    ->select('id','user_id','category_id','name','pattern','color','image','min_price','max_price','discount')
                    ->orderBy('id','DESC')
                    ->where('status', 1);
    if (!empty($keyword)) {
        $query->where(function ($q) use ($keyword) {
            $q->where('name', 'LIKE', '%' . $keyword . '%');
        });
    }
     $lists = $query->paginate($show_page);
     $count_product = Product::where('status', 1)->count();
    return response()->json([
            'product' =>  $lists,
             'count_product' =>  $count_product,
            'status' => 'success'
        ],200);
    }
}
