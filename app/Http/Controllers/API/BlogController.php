<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Blog;
use Carbon\Carbon;

class BlogController extends Controller
{
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++   list of all record +++++++++++++++++++++++++++++++++++++++++++++++++++++++
    public function index(Request $request){
        $title = isset($request->title) ? $request->title :'All Blogs';
        $show_page = isset($request->limit) ? $request->limit : 20;
        $query = Blog::orderBy('id','DESC')
             ->where('status', 1);

          if($title == 'Latest Blogs'){
              $query->where(function ($q) {
                  $q->whereDate('created_at', '>', Carbon::now()->subDays(30));
              });
          }
        if($title == 'undefined'){
            $title = 'All Blogs';
         }
        $lists = $query->paginate($show_page);
        $count_blog = Blog::where('status', 1)->count();
        return response()->json([
                'blog' =>  $lists,
                'count_blog' =>  $count_blog,
                'title' => $title,
                'status' => 'success'
        ],200);
   }

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++   show one record  +++++++++++++++++++++++++++++++++++++++++++++++++++++++
   public function show(Request $request){
        $data = Blog::where('status', 1)->find($request->id);
        if($data){
            return response()->json([
                'blog' =>   $data,
                'status' => 'success'
         ],200);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Record not found !'
          ],400);
        }       
    }
}
