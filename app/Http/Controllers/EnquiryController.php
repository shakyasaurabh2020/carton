<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Enquiry;

class EnquiryController extends Controller
{
      //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++    list     +++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
   public function index(Request $request )
   {
    //    $sort = isset($request->sort) ? $request->sort : 'id';
    //    $direction = isset($request->direction) ? $request->direction : 'desc';
       $keyword = isset($request->search) ? $request->search : '';
       $show_page = isset($request->limit) ? $request->limit : 5;
       $query = Enquiry::with('user','product')->select('id','user_id','product_id','message')
                   ->orderBy('id', 'desc');
            //    ->orderBy($sort,  $direction );
       if (!empty($keyword)) {
           $query->where(function ($q) use ($keyword) {
                   $q->where('message', 'LIKE', '%' . $keyword . '%');
               });
        //   $query->whereHas('product', function ($q) use ($keyword){
        //        $q->where('name', 'LIKE', '%' . $keyword . '%');
        //      });
        }
       $lists = $query->paginate($show_page);
       $lists->appends(['search' => $keyword]);
       return view('enquiry.enquiry')->with(compact('lists', 'keyword', 'show_page'));
   }

}
