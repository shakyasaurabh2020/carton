<?php

namespace App\Http\Controllers;
use App\User;
use App\Product;

class HomeController extends Controller
{
  
    public function __construct()
    {
        $this->middleware('auth');
    }

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++   dashboard  +++++++++++++++++++++++++++++++++++++++++++++++++++++++
    public function index()
    {
        $count_users = User::where('role_id',2)->count();
        $count_sellers = User::where('role_id',3)->count();
        $count_product = Product::count();
        return view('dashboard')->with(compact('count_users', 'count_sellers','count_product'));  
    }
}
