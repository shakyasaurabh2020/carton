<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use App\ProductImage;
use Illuminate\Http\Request;
use Auth;
use DB;

class ProductController extends Controller
{
  //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++    list     +++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
    public function index(Request $request)
    {

        $sort = isset($request->sort) ? $request->sort : 'id';
        $direction = isset($request->direction) ? $request->direction : 'desc';
        $keyword = isset($request->search) ? $request->search : '';
        $show_page = isset($request->limit) ? $request->limit : 5;
        $query = Product::with('category','user')->select('id', 'name','image','user_id','category_id','min_price','max_price','status','feature')
              ->orderBy($sort,  $direction );
        if (!empty($keyword)) {
            $query->where(function ($q) use ($keyword) {
                $q->where('name', 'LIKE', '%' . $keyword . '%');
            });
        }
        $lists = $query->paginate($show_page);
        $lists->appends(['search' => $keyword]);
        return view('product.product')->with(compact('lists', 'keyword', 'show_page'));
    }


   //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++    create     +++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
    public function create()
    {
        $data = null;
        $category = Category::select('id', 'name')->get();
        $dimension_unit = Product::$dimension_unit;
        return view('product.create-edit')->with(compact('data','category','dimension_unit'));
    }

  //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++    store     +++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
    public function store(Request $request)
      {
        $request->validate([
            'category_id' => 'required|numeric',
            'name' => 'required',
            'color' => 'required',
            'min_price' => 'required|numeric',
            'max_price' => 'required|numeric',
            // 'discount' => 'required|numeric',
            'pattern' => 'required',
            'sku' => 'required',
            'description' => 'required',
            // 'length' => 'required|numeric',
            // 'width' => 'required|numeric',
            // 'height' => 'required|numeric',
            'unit' => 'required',
            'weight' => 'required|numeric',
            'photo' => 'required|image|mimes:jpeg,png,jpg,svg|max:1024',
            'gallery_images' => 'required|array|between:1,7',
            'gallery_images.*' => 'image|mimes:jpg,jpeg,png,svg|max:20000',
           ]);
           $user_id = Auth::id();
           DB::beginTransaction();
        try {
            $folder_name = '/upload_images/product_images/';
          if ($request->hasFile('photo')) {
                $file = $request->file('photo');
                $image_new_name = rand() . '.' . $file->getClientOriginalExtension();
               
                $destinationPath = public_path() . '/upload_images/product_images';
                if (!file_exists('upload_images/product_images')) {
                    mkdir('upload_images/product_images', 0777, true);
                }
                $upload = $file->move($destinationPath, $image_new_name);
                $request->request->add(['image' =>  $folder_name.$image_new_name]); 
            }
            $request->request->add(['user_id' =>  $user_id ]); 
            $input = $request->except(['photo','gallery_images']);
            $store = Product::create($input);
           if($store){ 
               $product_id = $store->id;
            if ($request->hasFile('gallery_images')) {
                    foreach ($request->gallery_images as $key => $image) {                           
                        $file = $image;
                        $image_new_name = rand() . '.' . $file->getClientOriginalExtension();
                        $destinationPath =  public_path() . '/upload_images/product_images';
                        if (!file_exists('upload_images/product_images')) {
                            mkdir('upload_images/product_images', 0777, true);
                        }
                        $upload = $file->move($destinationPath, $image_new_name);
                        $store = ProductImage::create([
                            'product_id' => $product_id,
                            'image' => $folder_name.$image_new_name,
                          ]);  
                      }
                  }
                DB::commit(); 
                return back()->with('success','Record created successfully!'); 
             }
        } catch (\Exception $e) {
            DB::rollback();
             return redirect()->back()->with('error',  $e->getMessage());
        } catch (\Throwable $e) {
            DB::rollback();
             return redirect()->back()->with('error',  $e->getMessage());
        }
    }


//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++    show     +++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
    public function show($id)
      {
        $data = Product::with('product_images')->find($id);
        $category = Category::select('id', 'name')->get();
        $dimension_unit = Product::$dimension_unit;
        
        if($data){
            return view('product.create-edit')->with(compact('data','category','dimension_unit')); 
        } else {
             return redirect()->back()->with('error', 'No record found!');
        }   
    }



  //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++    update     +++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
    public function update(Request $request,  $id)
    {
        $data = Product::find($id);
        if($data){
            $request->validate([
                'category_id' => 'required|numeric',
                'name' => 'required',
                'color' => 'required',
                'min_price' => 'required|numeric',
                'max_price' => 'required|numeric',
                // 'discount' => 'required|numeric',
                'pattern' => 'required',
                'sku' => 'required',
                'description' => 'required',
                // 'length' => 'required|numeric',
                // 'width' => 'required|numeric',
                // 'height' => 'required|numeric',
                'unit' => 'required',
                'weight' => 'required|numeric',
                // 'photo' => Rule::requiredIf( function () use ( $data){
                //     return  $data->image == null;
                //      }),
                // 'gallery_images' => 'required|array|between:1,7',
                // 'gallery_images.*' => 'image|mimes:jpg,jpeg,png,svg|max:20000',
               ]);
               $user_id = Auth::id();
               DB::beginTransaction();
            try {
                $folder_name = '/upload_images/product_images/';
              if ($request->hasFile('photo')) {
                    $file = $request->file('photo');
                    $image_new_name = rand() . '.' . $file->getClientOriginalExtension();               
                    $destinationPath = public_path() . '/upload_images/product_images';
                    if (!file_exists('upload_images/product_images')) {
                        mkdir('upload_images/product_images', 0777, true);
                    }
                    $upload = $file->move($destinationPath, $image_new_name);
                    $request->request->add(['image' =>    $folder_name.$image_new_name]); 
                }
                $request->request->add(['user_id' =>  $user_id ]); 
                $input = $request->except(['photo','gallery_images']);
                $update =  $data->update($input);
               if($update){ 
                if ($request->hasFile('gallery_images')) {
                        foreach ($request->gallery_images as $key => $image) {                           
                            $file = $image;
                            $image_new_name = rand() . '.' . $file->getClientOriginalExtension();
                            $destinationPath =  public_path() . '/upload_images/product_images';
                            if (!file_exists('upload_images/product_images')) {
                                mkdir('upload_images/product_images', 0777, true);
                            }
                            $upload = $file->move($destinationPath, $image_new_name);
                            $store = ProductImage::create([
                                'product_id' => $id,
                                'image' => $folder_name.$image_new_name,
                              ]);  
                          }
                      }
                    DB::commit(); 
                    return back()->with('success','Record updated successfully!'); 
                 }
            } catch (\Exception $e) {
                DB::rollback();
                 return redirect()->back()->with('error',  $e->getMessage());
            } catch (\Throwable $e) {
                DB::rollback();
                 return redirect()->back()->with('error',  $e->getMessage());
            }
        } else {
             return redirect()->back()->with('error', 'No record found!');
        }
    }

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++    delete     +++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
    public function destroy($id)
    {
        $find = Product::find($id);
        if ($find) {   
            $find->product_images()->delete();   // delete related image of product  
            unlink(public_path() . $find->image);
            $find ->delete();
            return redirect()->back()->with('success','Record deleted successfully!');
        } else {   
            return redirect()->back()->with('error','No record found!');
        }
    }


// ==================   ajax call for toggle button ======================
    public function update_toggle(Request $request)
    {
        $id = $request->id;
        $result = Product::find($id);
        if ($result) {
            $update_status = Product::where('id', $id)->update([
                'status' => $request->status,
            ]);
            if ($update_status) {           
                return response()->json([
                    'message' => 'status updated successfully!',
                    'status' => 'success',
                ],200);
            } else {  
                return response()->json([
                    'message' => 'Oops! something went wrong!',
                    'status' => 'error',
                ],403);
            }
        } else {
            return response()->json([
                'message' => 'No record found!',
                'status' => 'error',
            ],403);
        }
    }


    // ==================   ajax call for toggle button  for product feature ======================
    public function feature_toggle(Request $request)
    {
        $id = $request->id;
        $result = Product::find($id);
        if ($result) {
            $update_status = Product::where('id', $id)->update([
                'feature' => $request->feature,
            ]);
            if ($update_status) {           
                return response()->json([
                    'message' => 'status updated successfully!',
                    'status' => 'success',
                ],200);
            } else {  
                return response()->json([
                    'message' => 'Oops! something went wrong!',
                    'status' => 'error',
                ],403);
            }
        } else {
            return response()->json([
                'message' => 'No record found!',
                'status' => 'error',
            ],403);
        }
    }
    
//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++    destroy_product_gallery     +++++++++++++++++++++++++++++++++++++++++++++++++++++++ 

    public function destroy_product_gallery($id)
    {
        $find = ProductImage::find($id);
        if ($find) {
            unlink(public_path() . $find->image);
            $find->delete();         
            return redirect()->back()->with('success','Record deleted successfully!');
        } else {   
            return redirect()->back()->with('error','No record found!');
        }
    }
}
