<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileRequest;
use App\Http\Requests\PasswordRequest;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    //  +++++++++++++++++++++++++++++++++++++++++++++++++++++    show  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
    public function edit()
    {
        return view('profile.edit');
    }

    
  //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++    update   +++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
    public function update(ProfileRequest $request)
    {
        $profile_update = auth()->user()->update($request->all());
        if($profile_update){
            return back()->with('success','Profile successfully updated!'); 
        } else {
            return back()->with('error','Oops! something went wrong!'); 
        }    
        // return back()->withStatus(__('Profile successfully updated.'));
    }


//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++    change password  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
    public function password(PasswordRequest $request)
    {
        $password = auth()->user()->update(['password' => Hash::make($request->get('password'))]);
        if($password){
            return back()->with('success','Password successfully updated!'); 
        } else {
            return back()->with('error','Oops! something went wrong!'); 
        }  
        // return back()->withStatusPassword(__('Password successfully updated.'));
    }
}
