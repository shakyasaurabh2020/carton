<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class BlogController extends Controller
{
   
   //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++    list     +++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
   public function index(Request $request )
   {

       $sort = isset($request->sort) ? $request->sort : 'id';
       $direction = isset($request->direction) ? $request->direction : 'desc';
       $keyword = isset($request->search) ? $request->search : '';
       $show_page = isset($request->limit) ? $request->limit : 5;
       $query = Blog::select('id', 'title','image','status')
               ->orderBy($sort,  $direction );
       if (!empty($keyword)) {
           $query->where(function ($q) use ($keyword) {
               $q->where('title', 'LIKE', '%' . $keyword . '%');
           });
       } 
       $lists = $query->paginate($show_page);
       $lists->appends(['search' => $keyword]);
       return view('blog.blog')->with(compact('lists', 'keyword', 'show_page'));
   }


//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++    create       +++++++++++++++++++++++++++++++++++++++++++++++++++++++
   public function create()
   {
       $data = null;
       return view('blog.create-edit')->with(compact('data'));
   }


//  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++      store         ++++++++++++++++++++++++++++++++++++++++++++++++++
   public function store(Request $request )
   {
       $request->validate([
           'title' => 'required',
           'body' => 'required',
           'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1024'
          ]);
       try {
         if ($request->hasFile('photo')) {
               $file = $request->file('photo');
               $image_new_name = rand() . '.' . $file->getClientOriginalExtension();
               $folder_name = '/upload_images/blog_images/';
               $destinationPath = public_path() . '/upload_images/blog_images';
               if (!file_exists('upload_images/blog_images')) {
                   mkdir('upload_images/blog_images', 0777, true);
               }
               $upload = $file->move($destinationPath, $image_new_name);
               $request->request->add(['image' =>   $folder_name.$image_new_name]); 
           }
           $input = $request->except(['photo']);
           $store = Blog::create($input);
          if($store){  
               return back()->with('success','Record created successfully!'); 
          } else {
              return redirect()->back()->with('error', 'Oops! something went wrong!');
            }
       } catch (\Exception $e) {
            return redirect()->back()->with('error',  $e->getMessage());
       } catch (\Throwable $e) {
            return redirect()->back()->with('error',  $e->getMessage());
       }
   }



//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++     show          +++++++++++++++++++++++++++++++++++++++++++++++++++++
   public function show($id)
   {
       $data = Blog::find($id);
       if($data){
           return view('blog.create-edit')->with(compact('data'));
       } else {
            return redirect()->back()->with('error', 'No record found!');
       }
   }



//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++      update          +++++++++++++++++++++++++++++++++++++++++++++++++++++
 
   public function update(Request $request, $id)
   {
       $data = Blog::find($id);
       if($data){
           $request->validate([
                'title' => 'required',
                'body' => 'required',
               'photo' => Rule::requiredIf( function () use ( $data){
                   return  $data->image == null;
                    }),
              ]);
           try {
             if ($request->hasFile('photo')) {
                   $file = $request->file('photo');
                   $image_new_name = rand() . '.' . $file->getClientOriginalExtension();
                   $folder_name = '/upload_images/blog_images/';
                   $destinationPath = public_path() . '/upload_images/blog_images';
                   if (!file_exists('upload_images/blog_images')) {
                       mkdir('upload_images/blog_images', 0777, true);
                   }
                   $upload = $file->move($destinationPath, $image_new_name);
                   $request->request->add(['image' =>  $folder_name.$image_new_name]); 
               }
               $input = $request->except(['photo']);
               $update =  $data->update($input);
              if($update){  
                   return back()->with('success','Record updated successfully!'); 
              } else {
                  return redirect()->back()->with('error', 'Oops! something went wrong!');
                }
           } catch (\Exception $e) {
                return redirect()->back()->with('error',  $e->getMessage());
           } catch (\Throwable $e) {
                return redirect()->back()->with('error',  $e->getMessage());
           }
       } else {
            return redirect()->back()->with('error', 'No record found!');
       }
   }



//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++       delete         +++++++++++++++++++++++++++++++++++++++++++++++++++++
   public function destroy($id)
   {
         $find = Blog::find($id);
       if ($find) {
           unlink(public_path() . $find->image);
           $find ->delete();
           return redirect()->back()->with('success','Record deleted successfully!');
       } else {   
           return redirect()->back()->with('error','No record found!');
       }
   }


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ajax call for toggle button ++++++++++++++++++++++++++++++++++++++++++++++++++++++++    
public function update_toggle(Request $request)
{
    $id = $request->id;
    $result = Blog::find($id);
    if ($result) {
        $update_status = Blog::where('id', $id)->update([
            'status' => $request->status,
        ]);
        if ($update_status) {           
            return response()->json([
                'message' => 'status updated successfully!',
                'status' => 'success',
            ],200);
        } else {  
            return response()->json([
                'message' => 'Oops! something went wrong!',
                'status' => 'error',
            ],403);
        }
    } else {
        return response()->json([
            'message' => 'No record found!',
            'status' => 'error',
        ],403);
    }
}

}
