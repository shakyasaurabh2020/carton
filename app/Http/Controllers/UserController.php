<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Country;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class UserController extends Controller
{
   
   //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++    list     +++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
   public function index(Request $request )
   {
       $sort = isset($request->sort) ? $request->sort : 'id';
       $direction = isset($request->direction) ? $request->direction : 'desc';
       $keyword = isset($request->search) ? $request->search : '';
       $show_page = isset($request->limit) ? $request->limit : 5;
       $query = User::select('id', 'name','email','profile_image','status')
                ->where('role_id', 2)
                ->orderBy($sort,  $direction );
       if (!empty($keyword)) {
           $query->where(function ($q) use ($keyword) {
               $q->where('name', 'LIKE', '%' . $keyword . '%')
                 ->orWhere('email', 'LIKE', '%' . $keyword . '%');
           });
       }
       $lists = $query->paginate($show_page);
       $lists->appends(['search' => $keyword]);
       return view('user.user')->with(compact('lists', 'keyword', 'show_page'));
   }


   //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++    create     +++++++++++++++++++++++++++++++++++++++++++++++++++++++   
    public function create()
    {
        $data = null;
        $country_list = Country::get();
        return view('user.create-edit')->with(compact('data','country_list'));
    }

   
  //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++    store     +++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
  public function store(Request $request)
  {
    $request->validate([
        'name' => 'required',
        'email' => 'required|email|unique:users,email',
        'user_password' => 'required',    
       ]);
    try {
      if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $image_new_name = rand() . '.' . $file->getClientOriginalExtension();
            $folder_name = '/upload_images/user_images/';
            $destinationPath = public_path() . '/upload_images/user_images';
            if (!file_exists('upload_images/user_images')) {
                mkdir('upload_images/user_images', 0777, true);
            }
            $upload = $file->move($destinationPath, $image_new_name);
            $request->request->add(['profile_image' =>  $folder_name.$image_new_name]); 
        }
        // $role_id = Role::select('id', 'name')->where('name','user')->first();
        $request->request->add(['role_id' => 2]); 
        if(isset($request->user_password)){
            $password =  bcrypt($request->user_password);
            $request->request->add(['password' => $password]); 
         }
        $input = $request->except(['photo']);
        $store = User::create($input);
       if($store){     
            return back()->with('success','Record created successfully!'); 
          }
        } catch (\Exception $e) {
            return redirect()->back()->with('error',  $e->getMessage());
        } catch (\Throwable $e) {
            return redirect()->back()->with('error',  $e->getMessage());
        }
    }

 //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++    show     +++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
    public function show($id)
    {
        $data = User::where('id', $id)->first();
        $country_list = Country::get();
        return view('user.create-edit')->with(compact('data','country_list'));
    }

    
  //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++    update     +++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
    public function update(Request $request, $id)
    {
        $data = User::find($id);
        if($data){
            $request->validate([
                'name' => 'required',
                'email' => 'required|email|unique:users,email,' . $id,
                'user_password' => 'required',    
               ]);
            try {
                if ($request->hasFile('photo')) {
                    $file = $request->file('photo');
                    $image_new_name = rand() . '.' . $file->getClientOriginalExtension();
                    $folder_name = '/upload_images/user_images/';
                    $destinationPath = public_path() . '/upload_images/user_images';
                    if (!file_exists('upload_images/user_images')) {
                        mkdir('upload_images/user_images', 0777, true);
                    }
                    $upload = $file->move($destinationPath, $image_new_name);
                    $request->request->add(['profile_image' =>  $folder_name.$image_new_name]); 
                }
                // $role_id = Role::select('id', 'name')->where('name','user')->first();
                // $request->request->add(['role_id' =>  $role_id->id]); 

                if(strcmp($request->get('user_password'), $data->password) != 0){ 
                        if(isset($request->user_password)){
                               $password =  bcrypt($request->user_password);
                               $request->request->add(['password' => $password]); 
                            }
                       }  
                $input = $request->except(['photo','user_password']);
                $update =  $data->update($input);
               if($update){     
                    return back()->with('success','Record updated successfully!'); 
                  }
            } catch (\Exception $e) {
                DB::rollback();
                 return redirect()->back()->with('error',  $e->getMessage());
            } catch (\Throwable $e) {
                DB::rollback();
                 return redirect()->back()->with('error',  $e->getMessage());
            }
        } else {
             return redirect()->back()->with('error', 'No record found!');
        }



        // $hasPassword = $request->get('password');
        // $user->update(
        //     $request->merge(['password' => Hash::make($request->get('password'))])
        //         ->except([$hasPassword ? '' : 'password']
        // ));
        // return redirect()->route('user.index')->withStatus(__('User successfully updated.'));
    }

  //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++    delete     +++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
    public function destroy($id)
    {
        $find = User::find($id);
        if ($find) {   
            unlink(public_path() . $find->image);
            $find ->delete();
            return redirect()->back()->with('success','Record deleted successfully!');
        } else {   
            return redirect()->back()->with('error','No record found!');
        }   
    }


 //  ++++++++++++++++++++++++++++++++++++++++++++++++++++++     ajax call for toggle button   +++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
    public function update_toggle(Request $request)
    {
        $id = $request->id;
        $result = User::find($id);
        if ($result) {
            $update_status = User::where('id', $id)->update([
                'status' => $request->status,
            ]);
            if ($update_status) {           
                return response()->json([
                    'message' => 'status updated successfully!',
                    'status' => 'success',
                ],200);
            } else {  
                return response()->json([
                    'message' => 'Oops! something went wrong!',
                    'status' => 'error',
                ],403);
            }
        } else {
            return response()->json([
                'message' => 'No record found!',
                'status' => 'error',
            ],403);
        }
    }
}
