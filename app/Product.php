<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Product extends Model
{
    use Sortable;
    protected $fillable = [
        'user_id',
        'category_id',
        'name',
        'pattern',
        'color',
        'image',
        'min_price',
        'max_price',
        'discount',
        'description',
        'length',
        'width',
        'height',
        'weight',
        'sku',
        'unit', 
        'feature',
        'status',
        'created_at',
        'updated_at'
    ];
    public $sortable = ['name','min_price','min_price'];
    public static $dimension_unit = [
        'inch',
        'cm',
        'm',
        'ft'
     ];

    public function category(){
        return $this->belongsTo('App\Category', 'category_id','id');
    }

    public function product_images(){
        return $this->hasMany('App\ProductImage', 'product_id','id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'user_id','id');
    }
}
