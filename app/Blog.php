<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Blog extends Model
{
    use Sortable;
    
    protected $fillable = [
        'title',
        'body',
        'image',
        'status'
    ];
    public $sortable = ['title'];
  
   
}
