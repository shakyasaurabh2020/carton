<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMeta extends Model
{
    protected $table = 'user_meta';
    protected $fillable = ['user_id',
         'company_name',
         'main_products',
         'total_employees',
         'registered_address',
         'operational_address',
         'business_type',
         'website',
         'est_year'
        ];
}
