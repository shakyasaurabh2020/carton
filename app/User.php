<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Kyslik\ColumnSortable\Sortable;
// use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable
{
    use Notifiable, Sortable;

    protected $fillable = [
          'role_id',
          'name',
          'email',
          'password',
          'mobile',
          'profile_image',
          'email_verified_at',
          'city',
          'state_id',
          'country_id',
          'country',
          'pincode',
          'status',
          'latitude',
          'longitude',
          'remember_token'
    ];

    public $sortable = ['name','email'];

    /**
     * The attributes that should be hidden for arrays.
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     */

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function user_meta(){
        return $this->belongsTo('App\UserMeta', 'id', 'user_id');
    }

    public function country(){
        return $this->belongsTo('App\Country','country_id', 'id');
    }


 
}
