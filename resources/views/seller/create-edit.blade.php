@extends('layouts.app', ['activePage' => 'seller', 'titlePage' =>'Create Seller'])
{{-- @section('page-css')
   
@stop --}}

@section('content')
<style>
    .form-group input[type=file] {
    opacity: 1;
    position: inherit;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 0;
}
</style>

  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
            @if(isset($data->id))
                <form method="post" class="form-horizontal" action="{{ route('update.seller',  $data->id) }}" autocomplete="off" enctype="multipart/form-data">
             @else
                <form method="post" class="form-horizontal" action="{{ route('store.seller') }}" autocomplete="off" enctype="multipart/form-data">
            @endif
            @csrf
            <div class="card ">
              <div class="card-header card-header-danger">
                <h4 class="card-title">
                  <b>
                    <i class="fa fa-list"> </i>
                      @if(isset($data->id))
                        Edit Seller
                      @else
                        Add New Seller
                      @endif 
                 </b>
              </h4>   
                <p class="card-category"></p>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-md-12 text-right">
                      <a href="{{ route('seller') }}" class="btn btn-sm btn-danger">{{ __('Back to list') }}</a>
                  </div>
                </div>

                <div class="row">
                    <label class="col-sm-2 col-form-label">Name <span class="req">*</span></label>
                    <div class="col-sm-7">
                      <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                        <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="input-name" type="text"
                          value="{{ old('name', isset($data->name) ? $data->name : null) }}" required/>
                        @if ($errors->has('name'))
                          <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <label class="col-sm-2 col-form-label">Email <span class="req">*</span></label>
                    <div class="col-sm-7">
                      <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                        <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="input-email" type="email"
                          value="{{ old('email', isset($data->email) ? $data->email : null) }}"  autocomplete="off" required/>
                        @if ($errors->has('email'))
                          <span id="email-error" class="error text-danger" for="input-email">{{ $errors->first('email') }}</span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <label class="col-sm-2 col-form-label">Mobile</label>
                    <div class="col-sm-7">
                      <div class="form-group{{ $errors->has('mobile') ? ' has-danger' : '' }}">
                        <input class="form-control{{ $errors->has('mobile') ? ' is-invalid' : '' }}" name="mobile" id="input-mobile" type="number"
                          value="{{ old('mobile', isset($data->mobile) ? $data->mobile : null) }}"/>
                        @if ($errors->has('mobile'))
                          <span id="name-error" class="error text-danger" for="input-mobile">{{ $errors->first('mobile') }}</span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <label class="col-sm-2 col-form-label">Password <span class="req">*</span></label>
                    <div class="col-sm-7">
                      <div class="form-group{{ $errors->has('user_password') ? ' has-danger' : '' }}">
                        <input class="form-control{{ $errors->has('user_password') ? ' is-invalid' : '' }}" name="user_password" id="input-user_password" type="password"
                          value="{{ old('user_password', isset($data->password) ? $data->password : null) }}"  autocomplete="off"  required/>
                        @if ($errors->has('user_password'))
                          <span id="user_password-error" class="error text-danger" for="input-user_password">{{ $errors->first('user_password') }}</span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <label class="col-sm-2 col-form-label">Country
                      <!-- <span class="req">*</span> -->
                    </label>
                    <div class="col-sm-7">
                      <select class="form-control" name="country_id">
                        <option value="">&nbsp; Select Country</option>
                        @foreach($country_list as $key => $list)
                        <option value="{{ $list->id}}" @if($data != null)
                            @if($list->id == $data->country_id) selected="true" @endif @endif
                            {{old('country_id') == ($list->id) ? 'selected' : '' }}>
                            &nbsp; {{ $list->name}}
                        </option>
                      @endforeach
                     </select>
                      @if($errors->has('country_id'))
                      <!-- <span class="error text-danger"> -->
                      <span id="country-error" class="error text-danger" for="input-country">{{ $errors->first('country_id') }}</span>
                      <!-- </span> -->
                      @endif
                    </div>
                  </div>
                  <div class="row">
                    <label class="col-sm-2 col-form-label">City </label>
                    <div class="col-sm-7">
                      <div class="form-group{{ $errors->has('city') ? ' has-danger' : '' }}">
                        <input class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" id="input-city" type="text"
                          value="{{ old('city', isset($data->city) ? $data->city : null) }}"/>
                        @if ($errors->has('city'))
                          <span id="city-error" class="error text-danger" for="input-city">{{ $errors->first('city') }}</span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <!-- <div class="row">
                    <label class="col-sm-2 col-form-label">Country</label>
                    <div class="col-sm-7">
                      <div class="form-group{{ $errors->has('country') ? ' has-danger' : '' }}">
                        <input class="form-control{{ $errors->has('country') ? ' is-invalid' : '' }}" name="country" id="input-country" type="text"
                          value="{{ old('country', isset($data->country) ? $data->country : null) }}"/>
                        @if ($errors->has('country'))
                          <span id="country-error" class="error text-danger" for="input-country">{{ $errors->first('country') }}</span>
                        @endif
                      </div>
                    </div>
                  </div> -->
                  <div class="row">
                    <label class="col-sm-2 col-form-label">Pincode</label>
                    <div class="col-sm-7">
                      <div class="form-group{{ $errors->has('pincode') ? ' has-danger' : '' }}">
                        <input class="form-control{{ $errors->has('pincode') ? ' is-invalid' : '' }}" name="pincode" id="input-pincode" type="text"
                          value="{{ old('pincode', isset($data->pincode) ? $data->pincode : null) }}"/>
                        @if ($errors->has('pincode'))
                          <span id="pincode-error" class="error text-danger" for="input-pincode">{{ $errors->first('pincode') }}</span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <label class="col-sm-2 col-form-label">Latitude</label>
                    <div class="col-sm-7">
                      <div class="form-group{{ $errors->has('latitude') ? ' has-danger' : '' }}">
                        <input class="form-control{{ $errors->has('latitude') ? ' is-invalid' : '' }}" name="latitude" id="input-latitude" type="text"
                          value="{{ old('latitude', isset($data->latitude) ? $data->latitude : null) }}"/>
                        @if ($errors->has('latitude'))
                          <span id="latitude-error" class="error text-danger" for="input-latitude">{{ $errors->first('latitude') }}</span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <label class="col-sm-2 col-form-label">Longitude</label>
                    <div class="col-sm-7">
                      <div class="form-group{{ $errors->has('longitude') ? ' has-danger' : '' }}">
                        <input class="form-control{{ $errors->has('longitude') ? ' is-invalid' : '' }}" name="longitude" id="input-longitude" type="text"
                          value="{{ old('longitude', isset($data->longitude) ? $data->longitude : null) }}"/>
                        @if ($errors->has('longitude'))
                          <span id="longitude-error" class="error text-danger" for="input-longitude">{{ $errors->first('longitude') }}</span>
                        @endif
                      </div>
                    </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label" for="input-photo">Profile Image</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('photo') ? ' has-danger' : '' }}">
                        <img @if(($data !=null) && ($data->profile_image != null)) src="{{ URL::to($data->profile_image) }}" @else
                          src="{{URL::to('/images/no_photo_available.png')}}" @endif
                          id="viewIMG" width="100" height="100" />
                         <br>
                       <input  type="file" name="photo"
                          accept="image/*" onchange="document.getElementById('viewIMG').src = window.URL.createObjectURL(this.files[0])"/>
                      @if($errors->has('photo'))
                        <span id="photo-error" class="error text-danger" for="input-photo">{{ $errors->first('photo') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-danger">Save</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection