@extends('layouts.app', ['activePage' => 'seller', 'titlePage' =>'Seller'])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-danger">
            <h4 class="card-title">
              <i class="fa fa-group"></i> 
              <b>Seller Table</b>
                <span class="pull-right">                 
                  <a  href="{{route('create.seller')}}">
                       <button type="submit" class="btn btn-sm text-danger" style="background-color: white;"> 
                          <i class="material-icons">add_circle_outline</i> 
                           Add New Seller
                       </button>
                    </a>
                    <a  href="{{route('seller')}}" rel="tooltip" title="Reload">
                      <i class="material-icons">cached</i>
                    </a>
                 </span>
             </h4>
          </div>
          <div class="card-body">
            <div class="row">
                <div class="col-md-8">
                </div> 
                <div class="col-md-4">
                    <form class="navbar-form" action="{{route('seller')}}">
                        <div class="input-group no-border">
                        <input type="search" @if($keyword) value="{{ $keyword}}"
                                @else value="{{ old('search') }}" @endif name="search"  class="form-control" placeholder="Search...">
                        <button type="submit" class="btn btn-white btn-round btn-just-icon">
                        <i class="material-icons">search</i>
                        <div class="ripple-container"></div>
                        </button>
                        </div>
                    </form>
                </div>
          </div>  
            <div class="table-responsive">
              <table class="table">
                <thead>
                {{-- <thead class="text-danger"> --}}
                  <th>#</th>
                  <th>@sortablelink('name','Name')</th>
                  <th>Image</th>
                  <th>@sortablelink('email','Email')</th>
                  <th>Status</th>
                  <th>Actions</th>
                </thead>
                <tbody>
                    @if(count($lists) > 0)
                    @foreach ($lists as $index => $list)
                     <tr>
                        <td>{{$lists->firstItem() + $index}}</td>
                        <td>{{$list->name}}</td>
                        <td><img class="img-circular" @if($list->profile_image != null) src="{{ URL::to($list->profile_image) }}" 
                           @else src="{{URL::to('/images/no_photo_available.png')}}" @endif width="50" height="50"></td>  
                        <td>{{$list->email}}</td> 
                        <td>
                          <input type="checkbox" id="toggleButton" class="js-switch js-switch-blue"
                              data-id="{{ $list->id }}" {{$list->status == 1 ? 'checked' : '' }} />
                        </td>      
                        <td class="td-actions">
                          <a href="{{ route('show.seller', $list->id) }}">
                               <button type="button" rel="tooltip" title="Edit" class="btn btn-success btn-link btn-sm">
                                <i class="material-icons">edit</i>
                              </button>
                           </a>
                           <a onclick="return confirm('Are you sure you want to delete this item?');" href="{{ route('delete.seller', $list->id) }}">
                                <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-link btn-sm">
                                  <i class="material-icons">close</i>
                                </button>
                            </a>
                        </td>        
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="12" class="text-center" style="color: red"><b>No records
                                found !!!</b></td>
                    </tr>
                    @endif
                </tbody> 
              </table>
              <div>
                {!! $lists->links() !!}
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js">  </script>
<script type="text/javascript">
$(document).ready(function(){
      $('.js-switch-blue').change(function () {
            let status = $(this).prop('checked') === true ? 1 : 0;
            let product_id = $(this).data('id');
            $.ajax({
              type: "POST",
              url: "{{ route('update_toggle.seller') }}",
               dataType: "json",
              data: {'status': status, 'id': product_id, _token: '{{ csrf_token() }}'},
              success: function (data) {
                  toastr.success(data.message);
              },
              error: function(data){
                  toastr.error(data.responseJSON.message);
              }
             });       
       });
  });
</script>
@endsection