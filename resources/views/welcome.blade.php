@extends('layouts.app', ['class' => 'off-canvas-sidebar', 'activePage' => 'welcome', 'title' => __('Material Dashboard')])
@section('content')
<div class="container" style="height: auto;">
  <div class="row justify-content-center">
      <div class="col-lg-7 col-md-8">
        <h1 class="text-white text-center">Front-end coming soon with Reactjs</h1>
      </div>
  </div>
</div>
@endsection

{{-- <!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Carton</title>
        <link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/all.css')}}" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="root"></div>
        <script src="{{asset('js/app.js')}}" ></script>
        <script src="{{asset('js/all.js')}}" ></script>
    </body>
</html> --}}
