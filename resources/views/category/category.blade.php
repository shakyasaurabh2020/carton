@extends('layouts.app', ['activePage' => 'category', 'titlePage' =>'Category'])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-danger">
            <h4 class="card-title">
              <i class="fa fa-list"></i> 
              <b>Category Table</b>
                <span class="pull-right">                 
                  <a  href="{{route('create.category')}}">
                       <button type="submit" class="btn btn-sm text-danger" style="background-color: white;"> 
                          <i class="material-icons">add_circle_outline</i> 
                           Add New Category
                       </button>
                    </a>
                    <a  href="{{route('category')}}" rel="tooltip" title="Reload">
                      <i class="material-icons">cached</i>
                    </a>
                 </span>
             </h4>
          </div>
          <div class="card-body">
            <div class="row">
                <div class="col-md-8">
                </div> 
                <div class="col-md-4">
                    <form class="navbar-form" action="{{route('category')}}">
                        <div class="input-group no-border">
                        <input type="search" @if($keyword) value="{{ $keyword}}"
                                @else value="{{ old('search') }}" @endif name="search"  class="form-control" placeholder="Search...">
                        <button type="submit" class="btn btn-white btn-round btn-just-icon">
                        <i class="material-icons">search</i>
                        <div class="ripple-container"></div>
                        </button>
                        </div>
                    </form>
                </div>
          </div>  
            {{-- <div class="row">
                <div class="col-md-12 text-right">
                    <a href="{{route('create.category')}}" class="btn btn-sm btn-info"> 
                        <i class="material-icons">add_circle_outline</i> 
                         Add New Category
                    </a>
                </div>
              </div> --}}

            <div class="table-responsive">
              <table class="table">
                <thead>
                {{-- <thead class="text-danger"> --}}
                  <th>#</th>
                  <th>@sortablelink('Name')</th>
                  <th>Image</th>
                  <th>Type</th>
                  <th>Actions</th>
                </thead>
                <tbody>
                    @if(count($lists) > 0)
                    @foreach ($lists as $index => $list)
                     <tr>
                        <td>{{$lists->firstItem() + $index}}</td>
                        <td>{{$list->name}}</td>
                        <td><img class="img-circular" src="{{ URL::to($list->image) }}" width="50" height="50"></td> 
                        <td> @if($list->parent_id == null) Category @else Sub-Category @endif </td>    
                        <td class="td-actions">
                          <a href="{{ route('show.category', $list->id) }}">
                               <button type="button" rel="tooltip" title="Edit" class="btn btn-success btn-link btn-sm">
                                <i class="material-icons">edit</i>
                              </button>
                           </a>
                           <a onclick="return confirm('Are you sure you want to delete this item?');" href="{{ route('delete.category', $list->id) }}">
                                <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-link btn-sm">
                                  <i class="material-icons">close</i>
                                </button>
                            </a>
                        </td>        
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="12" class="text-center" style="color: red"><b>No records
                                found !!!</b></td>
                    </tr>
                    @endif
                </tbody> 
              </table>
              <div>
                {!! $lists->links() !!}
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection