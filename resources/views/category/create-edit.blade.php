@extends('layouts.app', ['activePage' => 'category', 'titlePage' =>'Create Category'])
{{-- @section('page-css')
   
@stop --}}

@section('content')
<style>
    .form-group input[type=file] {
    opacity: 1;
    position: inherit;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 0;
}
</style>

  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
            {{-- @if(Session::has('flash_message'))
             <div class="row">
                <div class="col-sm-12">
                  <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <i class="material-icons">close</i>
                    </button>
                    <span> {{ Session::get('flash_message') }}</span>
                  </div>
                </div>
              </div>
            @endif --}}

            @if(isset($data->id))
                <form method="post" class="form-horizontal" action="{{ route('update.category',  $data->id) }}" autocomplete="off" enctype="multipart/form-data">
             @else
                <form method="post" class="form-horizontal" action="{{ route('store.category') }}" autocomplete="off" enctype="multipart/form-data">
            @endif
            @csrf
            {{-- @method('put') --}}
            <div class="card ">
              <div class="card-header card-header-danger">
                <h4 class="card-title">
                  <b>
                    <i class="fa fa-list"> </i>
                      @if(isset($data->id))
                        Edit Category
                      @else
                        Add New Category
                      @endif 
                 </b>
              </h4>
                <p class="card-category"></p>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-md-12 text-right">
                      <a href="{{ route('category') }}" class="btn btn-sm btn-danger">{{ __('Back to list') }}</a>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">Type <span class="req">*</span></label>
                  <div class="col-sm-7">
                    <label class="col-sm-3 radio-inline"> <input type="radio" name="type" value="0" 
                      {{ (old('type') == '0') ? 'checked' : '' }} @if($data != null){{ $data->parent_id == null ? 'checked' : '' }} @endif>Category</label>
                    <label class="col-sm-4 radio-inline"> <input type="radio" name="type" value="1"
                      {{ (old('type') == '1') ? 'checked' : '' }} @if($data != null) {{ $data->parent_id != null ? 'checked' : '' }} @endif>Sub Category</label>
                  </div>
                </div>
           {{-- category --}}           
                  <div class="row" id="category_dropdown" style="display: none;">
                    <label class="col-sm-2 col-form-label">Main Category<span class="req">*</span></label>
                    <div class="col-sm-7">
                      <select class="form-control" name="parent_id">
                        <option value="">&nbsp; Select Category</option>
                        @foreach($category as $key => $list)
                        <option value="{{ $list->id}}" @if($data != null)
                            @if($list->id == $data->parent_id) selected="true" @endif @endif
                            {{old('parent_id') == ($list->id) ? 'selected' : '' }}>
                            &nbsp; {{ $list->name}}
                        </option>
                      @endforeach
                     </select>
                      @if($errors->has('parent_id'))
                      <span class="error text-danger">
                        The Main Category field is required when type is 'Sub Category'</span>
                      @endif
                    </div>
                  </div>
       {{-- end category --}}
                <div class="row">
                    <label class="col-sm-2 col-form-label">Name <span class="req">*</span></label>
                    <div class="col-sm-7">
                      <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                        <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="input-name" type="text"
                          value="{{ old('name', isset($data->name) ? $data->name : null) }}"/>
                        @if ($errors->has('name'))
                          <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                        @endif
                      </div>
                    </div>
                  </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">Description <span class="req">*</span></label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('description') ? ' has-danger' : '' }}">
                      <textarea class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" id="input-description"
                           type="text" required >{{ old('description', isset($data->description) ? $data->description : null) }} </textarea >
                      @if ($errors->has('description'))
                        <span id="description-error" class="error text-danger" for="input-description">{{ $errors->first('description') }}</span>
                      @endif
                    </div>
                  </div>
                </div>

                <div class="row">
                  <label class="col-sm-2 col-form-label" for="input-photo">Image <span class="req">*</span></label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('photo') ? ' has-danger' : '' }}">
                        <img @if($data !=null) src="{{ URL::to($data->image) }}" @else
                          src="{{URL::to('/images/no_photo_available.png')}}" @endif
                          id="viewIMG" width="100" height="100" />
                         <br>
                       <input  type="file" name="photo"
                          accept="image/*" onchange="document.getElementById('viewIMG').src = window.URL.createObjectURL(this.files[0])"
                           @if($data==null) required @endif/>
                      @if($errors->has('photo'))
                        <span id="photo-error" class="error text-danger" for="input-photo">{{ $errors->first('photo') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-danger">Save</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js">  </script>
  <script type="text/javascript">
    $(document).ready(function(){
    // check radio value on window load 
      var radio_val= $("input[name='type']:checked").val()
         if(radio_val == 0){
            $("#category_dropdown").hide();
           }
        if(radio_val == 1){
            $("#category_dropdown").show();
        }
     // end  

      $("input[name='type']").change(function(){
         if($(this).is(":checked") ){
             var val = $(this).val();
               if(val == 1){
                  $("#category_dropdown").show();
                  }
               if(val == 0){
                $("#category_dropdown").hide();
                }
              }
          });
      });   
  </script> 
@endsection