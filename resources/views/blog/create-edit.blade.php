@extends('layouts.app', ['activePage' => 'blof', 'titlePage' =>'Create Blog'])
{{-- @section('page-css')
   
@stop --}}

@section('content')
<style>
    .form-group input[type=file] {
    opacity: 1;
    position: inherit;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 0;
  }
    .bmd-label-static{
      top: 2px !important;
    }
</style>

  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
            @if(isset($data->id))
                <form method="post" class="form-horizontal" action="{{ route('update.blog',  $data->id) }}" autocomplete="off" enctype="multipart/form-data">
             @else
                <form method="post" class="form-horizontal" action="{{ route('store.blog') }}" autocomplete="off" enctype="multipart/form-data">
            @endif
            @csrf
            {{-- @method('put') --}}
            <div class="card ">
              <div class="card-header card-header-danger">
                <h4 class="card-title">
                  <b>
                    <i class="fa fa-list"> </i>
                      @if(isset($data->id))
                        Edit Blog
                      @else
                        Add New Blog
                      @endif 
                 </b>
              </h4>
                <p class="card-category"></p>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-md-12 text-right">
                      <a href="{{ route('blog') }}" class="btn btn-sm btn-danger">{{ __('Back to list') }}</a>
                  </div>
                </div>
              
                <div class="row">
                    <label class="col-sm-2 col-form-label">Title <span class="req">*</span></label>
                    <div class="col-sm-7">
                      <div class="form-group{{ $errors->has('title') ? ' has-danger' : '' }}">
                        <input class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" id="input-title" type="text"
                          value="{{ old('title', isset($data->title) ? $data->title : null) }}" required/>
                        @if ($errors->has('title'))
                          <span id="title-error" class="error text-danger" for="input-title">{{ $errors->first('title') }}</span>
                        @endif
                      </div>
                    </div>
                  </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">Body <span class="req">*</span></label>
                  <div class="col-sm-10">
                    <div class="form-group{{ $errors->has('body') ? ' has-danger' : '' }}">
                      <textarea class="summernote form-control{{ $errors->has('body') ? ' is-invalid' : '' }}" name="body" id="input-body"
                           type="text" required >{!! old('body', isset($data->body) ? $data->body : null) !!} </textarea >
                      @if ($errors->has('body'))
                        <span id="body-error" class="error text-danger" for="input-body">{{ $errors->first('body') }}</span>
                      @endif
                    </div>
                  </div>
                </div>

                <div class="row">
                  <label class="col-sm-2 col-form-label" for="input-photo">Image <span class="req">*</span></label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('photo') ? ' has-danger' : '' }}">
                        <img @if($data !=null) src="{{ URL::to($data->image) }}" @else
                          src="{{URL::to('/images/no_photo_available.png')}}" @endif
                          id="viewIMG" width="100" height="100" />
                         <br>
                       <input  type="file" name="photo"
                          accept="image/*" onchange="document.getElementById('viewIMG').src = window.URL.createObjectURL(this.files[0])"
                           @if($data==null) required @endif/>
                      @if($errors->has('photo'))
                        <span id="photo-error" class="error text-danger" for="input-photo">{{ $errors->first('photo') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-danger">Save</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js">  </script>     
  <script type="text/javascript">
    $(document).ready(function() {
         $('.summernote').summernote({
          height: 300,
          popover: {
              image: [],
              link: [],
              air: []
              }
       });
    });
</script>
@endsection