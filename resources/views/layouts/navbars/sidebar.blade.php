<div class="sidebar" data-color="danger" data-background-color="white" data-image="{{ asset('material') }}/img/sidebar-1.jpg">
  <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
  <div class="logo">
    <a href="javascript:;" class="simple-text logo-normal">
      <img src="{{URL::to('/images/logo.png')}}"  alt="Carton" style="margin-top: -20px;">
    </a>
  </div>
  <div class="sidebar-wrapper">
    <ul class="nav">
      <li class="nav-item{{ $activePage == 'dashboard' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('home') }}">
          <i class="material-icons">dashboard</i>
            <p>{{ __('Dashboard') }}</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'category' ? ' active' : '' }}">
        {{-- <li class="nav-item{{ $activePage == 'table' ? ' active' : '' }}"> --}}
        <a class="nav-link" href="{{ route('category') }}">
          <i class="material-icons">list</i>
            <p>Category</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'product' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('product') }}">
          <i class="fa fa-cube"> </i>
            <p>Product</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'user' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('user') }}">
          {{-- <i class="material-icons">people</i> --}}
          <i class="fa fa-user"></i>
            <p>User</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'seller' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('seller') }}">
          <i class="fa fa-group"></i>
          {{-- <i class="material-icons">business_cen</i> --}}
            <p>Seller</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'blog' ? ' active' : '' }}">
           <a class="nav-link" href="{{ route('blog') }}">
            <i class="material-icons">library_books</i>
            <p>Blog</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'enquiry' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('enquiry') }}"> 
        <!-- <a class="nav-link">  -->
          <i class="fa fa-envelope"></i>
            <p>Enquiry</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'help' ? ' active' : '' }}">
        {{-- <a class="nav-link" href="{{ route('blog') }}"> --}}
        <a class="nav-link"> 
          <i class="material-icons">question_answer</i>
            <p>Help Desk</p>
        </a>
      </li>

      <li class="nav-item {{ ($activePage == 'company_logo' || $activePage == 'contact_info'|| $activePage == 'about_us'
          || $activePage == 'terms_condition'|| $activePage == 'social_link' || $activePage == 'location') ? ' active' : '' }}">
        <a class="nav-link" data-toggle="collapse" href="#setting">
          <i class="material-icons">settings</i>
          <p>Settings
            <b class="caret"></b>
          </p>
        </a>
        <div class="collapse" id="setting">
          <ul class="nav">
            <li class="nav-item{{ $activePage == 'company_logo' ? ' active' : '' }}">
              <a class="nav-link">
                <span class="sidebar-mini"><i class="material-icons">camera</i> </span>
                <span class="sidebar-normal">Company & Logo</span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'contact_info' ? ' active' : '' }}">
              <a class="nav-link">
                <span class="sidebar-mini"><i class="material-icons">perm_contact_calendar</i> </span>
                <span class="sidebar-normal"> Contact Info </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'about_us' ? ' active' : '' }}">
              <a class="nav-link">
                <span class="sidebar-mini"><i class="material-icons">lens</i></span>
                <span class="sidebar-normal">About Us</span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'terms_condition' ? ' active' : '' }}">
              <a class="nav-link">
                <span class="sidebar-mini"><i class="material-icons">description</i></span>
                <span class="sidebar-normal"> Terms & Condition </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'social_link' ? ' active' : '' }}">
              <a class="nav-link">
                <span class="sidebar-mini"><i class="material-icons">link</i></span>
                <span class="sidebar-normal">Social Link</span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'location' ? ' active' : '' }}">
              <a class="nav-link">
                <span class="sidebar-mini"> <i class="material-icons">location_ons</i> </span>
                <span class="sidebar-normal">Service Providing Locations</span>
              </a>
            </li>
          </ul>
        </div>
      </li>


      {{-- <li class="nav-item{{ $activePage == 'table' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('table') }}">
          <i class="material-icons">content_paste</i>
            <p>{{ __('Table List') }}</p>
        </a>
      </li> --}}
      {{-- <li class="nav-item{{ $activePage == 'typography' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('typography') }}">
          <i class="material-icons">library_books</i>
            <p>{{ __('Typography') }}</p>
        </a>
      </li> --}}
      {{-- <li class="nav-item{{ $activePage == 'icons' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('icons') }}">
          <i class="material-icons">bubble_chart</i>
          <p>{{ __('Icons') }}</p>
        </a>
      </li> --}}
      {{-- <li class="nav-item{{ $activePage == 'map' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('map') }}">
          <i class="material-icons">location_ons</i>
            <p>{{ __('Maps') }}</p>
        </a>
      </li> --}}
      {{-- <li class="nav-item{{ $activePage == 'notifications' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('notifications') }}">
          <i class="material-icons">notifications</i>
          <p>{{ __('Notifications') }}</p>
        </a>
      </li> --}}
      {{-- <li class="nav-item{{ $activePage == 'language' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('language') }}">
          <i class="material-icons">language</i>
          <p>{{ __('RTL Support') }}</p>
        </a>
      </li> --}}
      {{-- <li class="nav-item active-pro{{ $activePage == 'upgrade' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('upgrade') }}">
          <i class="material-icons">unarchive</i>
          <p>{{ __('Upgrade to PRO') }}</p>
        </a>
      </li> --}}
    </ul>
  </div>
</div>