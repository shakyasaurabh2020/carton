<footer class="footer">
  <div class="container-fluid">
    <div class="copyright">
      &copy;
      <script>
        document.write(new Date().getFullYear())
      </script>, made with <i class="material-icons">favorite</i> by 
      <a href="" target="_blank" style="color: #322781;"> Imperial It Pvt Ltd </a>for a better web.
      {{-- // </script>, made with <i class="material-icons">favorite</i> by --}}
      {{-- // <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a> and <a href="https://www.updivision.com" target="_blank">UPDIVISION</a> for a better web. --}}
    </div>
  </div>
</footer>