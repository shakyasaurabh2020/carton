@extends('layouts.app', ['activePage' => 'enquiry', 'titlePage' =>'Enquiry'])
@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-danger">
            <h4 class="card-title">
              <i class="fa fa-cube"> </i>
                <b> Enquiry Table</b>
                <span class="pull-right">                 
                  <!-- <a  href="{{route('create.product')}}">
                       <button type="submit" class="btn btn-sm text-danger" style="background-color: white;"> 
                          <i class="material-icons">add_circle_outline</i> 
                           Add New Product
                       </button>
                    </a> -->
                    <a  href="{{route('enquiry')}}" rel="tooltip" title="Reload">
                      <i class="material-icons">cached</i>
                    </a>
                 </span>
             </h4>
          </div>
          <div class="card-body">
            <div class="row">
                <div class="col-md-8">
                </div> 
                <div class="col-md-4">
                    <form class="navbar-form" action="{{route('enquiry')}}">
                        <div class="input-group no-border">
                        <input type="search" @if($keyword) value="{{ $keyword}}"
                                @else value="{{ old('search') }}" @endif name="search"  class="form-control" placeholder="Search...">
                        <button type="submit" class="btn btn-white btn-round btn-just-icon">
                        <i class="material-icons">search</i>
                        <div class="ripple-container"></div>
                        </button>
                        </div>
                    </form>
                </div>
          </div>  
            <div class="table-responsive">
              <table class="table">
                  <thead>
                  <th>#</th>
                  <th>Sender Name</th>
                  <th>Product</th>
                  <th>Message</th>
                </thead>
                <tbody>
                    @if(count($lists) > 0)
                    @foreach ($lists as $index => $list)
                     <tr>
                        <td>{{$lists->firstItem() + $index}}</td>   
                        <td>{{$list['user']['name']}}</td>
                        <td>{{$list['product']['name']}}</td>    
                        <td>{{$list->message}}</td>       
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="12" class="text-center" style="color: red"><b>No records
                                found !!!</b></td>
                    </tr>
                    @endif
                </tbody> 
              </table>
              <div>
                {!! $lists->links() !!}
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js">  </script>     
<script type="text/javascript">
  $(document).ready(function(){
      $('.js-switch-blue').change(function () {
            let status = $(this).prop('checked') === true ? 1 : 0;
            let product_id = $(this).data('id');
            $.ajax({
              type: "POST",
              url: "{{ route('update_toggle.product') }}",
               dataType: "json",
              data: {'status': status, 'id': product_id, _token: '{{ csrf_token() }}'},
              success: function (data) {
                  toastr.success(data.message);
              },
              error: function(data){
                  toastr.error(data.responseJSON.message);
              }
             });       
      });

      $('.js-switch-feature').change(function () {
            let feature = $(this).prop('checked') === true ? 1 : 0;
            let product_id = $(this).data('id');
            $.ajax({
              type: "POST",
              url: "{{ route('feature_toggle.product') }}",
               dataType: "json",
              data: {'feature': feature, 'id': product_id, _token: '{{ csrf_token() }}'},
              success: function (data) {
                  toastr.success(data.message);
              },
              error: function(data){
                  toastr.error(data.responseJSON.message);
              }
             });       
      });
  });
   
  </script> -->
@endsection

