@extends('layouts.app', ['activePage' => 'product', 'titlePage' =>'Create Product'])
@section('content')
<style>
    .form-group input[type=file] {
    opacity: 1;
    position: inherit;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 0;
}
</style>

  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
            @if(isset($data->id))
                <form method="post" class="form-horizontal" action="{{ route('update.product',  $data->id) }}" autocomplete="off" enctype="multipart/form-data">
             @else
                <form method="post" class="form-horizontal" action="{{ route('store.product') }}" autocomplete="off" enctype="multipart/form-data">
            @endif
            @csrf
            {{-- @method('put') --}}
            <div class="card ">
              <div class="card-header card-header-danger">     
                <h4 class="card-title">
                  <b>
                    <i class="fa fa-cube"></i>
                      @if(isset($data->id))
                        Edit Product
                      @else
                        Add New Product
                      @endif 
                 </b>
              </h4>

              <p class="card-category"></p>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-md-12 text-right">
                      <a href="{{ route('product') }}" class="btn btn-sm btn-danger">{{ __('Back to list') }}</a>
                  </div>
                </div>       
                  <div class="row">
                    <label class="col-sm-2 col-form-label">Category <span class="req">*</span></label>
                    <div class="col-sm-7"> 
                      <select class="form-control" name="category_id"
                      required>
                      <option value="">&nbsp; Select Category</option>
                      @foreach($category as $key => $list)
                      <option value="{{ $list['id'] }}" @if($data !=null)
                          @if($list['id']==$data['category_id']) selected="true" @endif @endif
                          {{old('category_id') == ($list['id']) ? 'selected' : '' }}>
                          &nbsp; {{ $list['name']}}</option>
                      @endforeach
                    </select>
                    </div>
                  </div> 
                  <div class="row">
                    <label class="col-sm-2 col-form-label">Product Name <span class="req">*</span></label>
                    <div class="col-sm-7">
                      <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                        <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="input-name" type="text"
                          value="{{ old('name', isset($data->name) ? $data->name : null) }}" required/>
                        @if ($errors->has('name'))
                          <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <label class="col-sm-2 col-form-label">Color <span class="req">*</span></label>
                    <div class="col-sm-7">
                      <div class="form-group{{ $errors->has('color') ? ' has-danger' : '' }}">
                        <input class="form-control{{ $errors->has('color') ? ' is-invalid' : '' }}" name="color" id="input-color" type="text"
                          value="{{ old('color', isset($data->color) ? $data->color : null) }}" required/>
                        @if ($errors->has('color'))
                          <span id="color-error" class="error text-danger" for="input-color">{{ $errors->first('color') }}</span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <label class="col-sm-2 col-form-label">Min Price <span class="req">*</span></label>
                    <div class="col-sm-7">
                      <div class="form-group{{ $errors->has('min_price') ? ' has-danger' : '' }}">
                        <input class="form-control{{ $errors->has('min_price') ? ' is-invalid' : '' }}" name="min_price" id="input-min_price"  
                        type="number" step="any" value="{{ old('min_price', isset($data->min_price) ? $data->min_price : null) }}" required/>
                        @if ($errors->has('min_price'))
                          <span id="min_price-error" class="error text-danger" for="input-min_price">{{ $errors->first('min_price') }}</span>
                        @endif
                      </div>
                    </div>
                  </div>
            
                  <div class="row">
                    <label class="col-sm-2 col-form-label">Max Price <span class="req">*</span></label>
                    <div class="col-sm-7">
                      <div class="form-group{{ $errors->has('max_price') ? ' has-danger' : '' }}">
                        <input class="form-control{{ $errors->has('max_price') ? ' is-invalid' : '' }}" name="max_price" id="input-max_price"
                        type="number" step="any" value="{{ old('max_price', isset($data->max_price) ? $data->max_price : null) }}" required/>
                        @if ($errors->has('max_price'))
                          <span id="max_price-error" class="error text-danger" for="input-max_price">{{ $errors->first('max_price') }}</span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <label class="col-sm-2 col-form-label">Discount %</label>
                    <div class="col-sm-7">
                      <div class="form-group{{ $errors->has('discount') ? ' has-danger' : '' }}">
                        <input class="form-control{{ $errors->has('discount') ? ' is-invalid' : '' }}" name="discount" id="input-discount" 
                        type="number" value="{{ old('discount', isset($data->discount) ? $data->discount : null) }}" required/>
                        @if ($errors->has('discount'))
                          <span id="discount-error" class="error text-danger" for="input-discount">{{ $errors->first('discount') }}</span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <label class="col-sm-2 col-form-label">Pattern <span class="req">*</span></label>
                    <div class="col-sm-7">
                      <div class="form-group{{ $errors->has('pattern') ? ' has-danger' : '' }}">
                        <input class="form-control{{ $errors->has('pattern') ? ' is-invalid' : '' }}" name="pattern" id="input-pattern" type="text"
                          value="{{ old('pattern', isset($data->pattern) ? $data->pattern : null) }}" required/>
                        @if ($errors->has('pattern'))
                          <span id="pattern-error" class="error text-danger" for="input-pattern">{{ $errors->first('pattern') }}</span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <label class="col-sm-2 col-form-label">SKU (Stock Keeping Unit) <span class="req">*</span></label>
                    <div class="col-sm-7">
                      <div class="form-group{{ $errors->has('sku') ? ' has-danger' : '' }}">
                        <input class="form-control{{ $errors->has('sku') ? ' is-invalid' : '' }}" name="sku" id="input-sku" type="text"
                          value="{{ old('sku', isset($data->sku) ? $data->sku : null) }}" required/>
                        @if ($errors->has('sku'))
                          <span id="sku-error" class="error text-danger" for="input-sku">{{ $errors->first('sku') }}</span>
                        @endif
                      </div>
                    </div>
                  </div>

                <div class="row">
                  <label class="col-sm-2 col-form-label">Description <span class="req">*</span></label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('description') ? ' has-danger' : '' }}">
                      <textarea class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" id="input-description"
                           type="text" required >{{ old('description', isset($data->description) ? $data->description : null) }} </textarea >
                      @if ($errors->has('description'))
                        <span id="description-error" class="error text-danger" for="input-description">{{ $errors->first('description') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
    {{-- -------------------  Carton Dimensions   ------------------ --}}
                <div class="col-sm-12" style="background-color: whitesmoke;color: rgb(40, 30, 124);font-size: 17px;"> 
                  <div class="card-header "><i class="fa fa-cube"></i> Carton Dimensions </div>
                </div>
              
                <div class="row">
                  <label class="col-sm-2 col-form-label">Length</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('length') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('length') ? ' is-invalid' : '' }}" name="length" id="input-length" type="number"
                       step="any" value="{{ old('length', isset($data->length) ? $data->length : null) }}" />
                      @if ($errors->has('length'))
                        <span id="length-error" class="error text-danger" for="input-length">{{ $errors->length }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">Width</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('width') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('width') ? ' is-invalid' : '' }}" name="width" id="input-width"
                      type="number" step="any" value="{{ old('width', isset($data->width) ? $data->width : null) }}" />
                      @if ($errors->has('width'))
                        <span id="width-error" class="error text-danger" for="input-width">{{ $errors->first('width') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">Height</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('height') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('height') ? ' is-invalid' : '' }}" name="height" id="input-height"
                         type="number" step="any" value="{{ old('height', isset($data->height) ? $data->height : null) }}" />
                      @if ($errors->has('height'))
                        <span id="height-error" class="error text-danger" for="input-height">{{ $errors->first('height') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">Dimension Unit <span class="req">*</span></label>
                  <div class="col-sm-7">	
                    <select class="form-control" name="unit"
                      required>
                      <option value="">&nbsp; Select Dimension Unit</option>
                      @foreach($dimension_unit as $key => $list)
                      <option value="{{ $list }}" @if($data !=null)
                          @if($list==$data->unit) selected="true" @endif @endif
                          {{old('unit') == ($list) ? 'selected' : '' }}>
                          &nbsp; {{ $list}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">Weight (in KG) <span class="req">*</span></label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('weight') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('weight') ? ' is-invalid' : '' }}" name="weight" id="input-weight" 
                      type="number" step="any"  value="{{ old('weight', isset($data->weight) ? $data->weight : null) }}" required/>
                      @if ($errors->has('name'))
                        <span id="weight-error" class="error text-danger" for="input-weight">{{ $errors->first('weight') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                {{-- ---------------------   Product Image      ---------------- --}}
                <div class="col-sm-12" style="background-color: whitesmoke;color: rgb(40, 30, 124);font-size: 17px;">
                   <div class="card-header "><i class="fa fa-image"></i> Product Image</div>
                  </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label" for="input-photo">Image <span class="req">*</span></label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('photo') ? ' has-danger' : '' }}">
                        <img @if($data !=null) src="{{ URL::to($data->image) }}" @else
                          src="{{URL::to('/images/no_photo_available.png')}}" @endif
                          id="viewIMG" width="100" height="100" />
                         <br>
                       <input  type="file" name="photo"
                          accept="image/*" onchange="document.getElementById('viewIMG').src = window.URL.createObjectURL(this.files[0])"
                           @if($data==null) required @endif/>
                      @if($errors->has('photo'))
                        <span id="photo-error" class="error text-danger" for="input-photo">{{ $errors->first('photo') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                {{-- ------------------   Product Gallery        ------------------- --}}
                <div class="col-sm-12" style="background-color: whitesmoke;color: rgb(40, 30, 124);font-size: 17px;">
                   <div class="card-header "><i class="fa fa-image"></i> Product Gallery </div>
                 </div>
               <div class="row">
                 <label class="col-sm-2 col-form-label" for="input-gallery_images">Gallery Images <span class="req">*</span></label>
                 <div class="col-sm-7">
                   <div class="form-group{{ $errors->has('gallery_images') ? ' has-danger' : '' }}">
                      <input  type="file" name="gallery_images[]" id="upload_galleryIMG" 
                         accept="image/*" @if($data==null) required @endif multiple /> 
                         <br>
                     @if($errors->has('gallery_images'))
                       <span id="gallery_images-error" class="error text-danger" for="input-gallery_images">{{ $errors->first('gallery_images') }}</span>
                     @endif
                   </div>
                   <div id="image_preview"></div>
                 </div>
               </div>
             @if($data != null)
               <div class="row" style="margin-left: 10%;"> 
                @foreach ($data['product_images'] as $key => $image) 
                <div>
                    <a  onclick="return confirm('Are you sure you want to delete this item?');" href="{{ route('delete.product_gallery', $image->id) }}" 
                      data-id="{{ $image->id }}" class="delete delete-button-position"><i class="fa fa-trash-o" title="Delete"></i></a>                                                                                                                            
                    <img  class="mr-10"  src="{{ URL::to($image->image) }}" height="80" width="80" >
                </div>
                @endforeach 
              </div>
             @endif
          {{-- ---------------submit  button  ---------------------- --}}
              </div>
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-danger">Save</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js">  </script>
<script type="text/javascript">
$(document).ready(function(){
  $("#upload_galleryIMG").change(function(){
     $('#image_preview').html("");
     var total_file=document.getElementById("upload_galleryIMG").files.length;
     for(var i=0;i<total_file;i++)
     {
      $('#image_preview').append("<img width='100' height='100' style='margin-right: 2%;'src='"+URL.createObjectURL(event.target.files[i])+"'>");
     }
  });
 }); 

</script>
@endsection