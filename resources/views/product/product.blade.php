@extends('layouts.app', ['activePage' => 'product', 'titlePage' =>'Product'])
@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-danger">
            <h4 class="card-title">
              <i class="fa fa-cube"> </i>
                <b> Product Table</b>
                <span class="pull-right">                 
                  <a  href="{{route('create.product')}}">
                       <button type="submit" class="btn btn-sm text-danger" style="background-color: white;"> 
                          <i class="material-icons">add_circle_outline</i> 
                           Add New Product
                       </button>
                    </a>
                    <a  href="{{route('category')}}" rel="tooltip" title="Reload">
                      <i class="material-icons">cached</i>
                    </a>
                 </span>
             </h4>
          </div>
          <div class="card-body">
            <div class="row">
                <div class="col-md-8">
                </div> 
                <div class="col-md-4">
                    <form class="navbar-form" action="{{route('category')}}">
                        <div class="input-group no-border">
                        <input type="search" @if($keyword) value="{{ $keyword}}"
                                @else value="{{ old('search') }}" @endif name="search"  class="form-control" placeholder="Search...">
                        <button type="submit" class="btn btn-white btn-round btn-just-icon">
                        <i class="material-icons">search</i>
                        <div class="ripple-container"></div>
                        </button>
                        </div>
                    </form>
                </div>
          </div>  
            <div class="table-responsive">
              <table class="table">
                  <thead>
                  <th>#</th>
                  <th>@sortablelink('name','Name')</th>
                  <th>Image</th>
                  <th>Category</th>
                  <th>Seller Name</th>
                  <th>@sortablelink('min_price','Min Price')</th>
                  <th>@sortablelink('max_price','Max Price')</th>
                  <th>Feature</th>
                  <th>Status</th>
                  <th>Actions</th>
                </thead>
                <tbody>
                    @if(count($lists) > 0)
                    @foreach ($lists as $index => $list)
                     <tr>
                        <td>{{$lists->firstItem() + $index}}</td>
                        <td>{{$list->name}}</td>
                        <td><img class="img-circular" src="{{ URL::to($list->image) }}" width="50" height="50"></td>         
                        <td>{{$list['category']['name']}}</td>
                        <td>{{$list['user']['name']}}</td>
                        <td>${{$list->min_price}}</td> 
                        <td>${{$list->max_price}}</td>
                        <td>
                          <input type="checkbox" id="featureButton" class="js-switch js-switch-feature"
                              data-id="{{ $list->id }}" {{$list->feature == 1 ? 'checked' : '' }} />
                        </td>
                        <td>
                          <input type="checkbox" id="toggleButton" class="js-switch js-switch-blue"
                              data-id="{{ $list->id }}" {{$list->status == 1 ? 'checked' : '' }} />
                        </td>
                        <td class="td-actions">
                          <a href="{{ route('show.product', $list->id) }}">
                               <button type="button" rel="tooltip" title="Edit" class="btn btn-success btn-link btn-sm">
                                <i class="material-icons">edit</i>
                              </button>
                           </a>
                           <a onclick="return confirm('Are you sure you want to delete this item?');" href="{{ route('delete.product', $list->id) }}">
                                <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-link btn-sm">
                                  <i class="material-icons">close</i>
                                </button>
                            </a>
                        </td>        
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="12" class="text-center" style="color: red"><b>No records
                                found !!!</b></td>
                    </tr>
                    @endif
                </tbody> 
              </table>
              <div>
                {!! $lists->links() !!}
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js">  </script>     
<script type="text/javascript">
  $(document).ready(function(){
      $('.js-switch-blue').change(function () {
            let status = $(this).prop('checked') === true ? 1 : 0;
            let product_id = $(this).data('id');
            $.ajax({
              type: "POST",
              url: "{{ route('update_toggle.product') }}",
               dataType: "json",
              data: {'status': status, 'id': product_id, _token: '{{ csrf_token() }}'},
              success: function (data) {
                  toastr.success(data.message);
              },
              error: function(data){
                  toastr.error(data.responseJSON.message);
              }
             });       
      });

      $('.js-switch-feature').change(function () {
            let feature = $(this).prop('checked') === true ? 1 : 0;
            let product_id = $(this).data('id');
            $.ajax({
              type: "POST",
              url: "{{ route('feature_toggle.product') }}",
               dataType: "json",
              data: {'feature': feature, 'id': product_id, _token: '{{ csrf_token() }}'},
              success: function (data) {
                  toastr.success(data.message);
              },
              error: function(data){
                  toastr.error(data.responseJSON.message);
              }
             });       
      });
  });
   
  </script>
@endsection

