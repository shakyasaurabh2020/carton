<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');

Route::group(['middleware' => 'auth'], function () {
	Route::get('table-list', function () {
		return view('pages.table_list');
	})->name('table');

	Route::get('typography', function () {
		return view('pages.typography');
	})->name('typography');

	Route::get('icons', function () {
		return view('pages.icons');
	})->name('icons');

	Route::get('map', function () {
		return view('pages.map');
	})->name('map');

	Route::get('notifications', function () {
		return view('pages.notifications');
	})->name('notifications');

	Route::get('rtl-support', function () {
		return view('pages.language');
	})->name('language');

	Route::get('upgrade', function () {
		return view('pages.upgrade');
	})->name('upgrade');
});

     Route::group(['prefix' => 'admin'], function () {
	  // =========================   AuthController    ======================================================
		Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
		Route::post('/login', 'Auth\LoginController@login');
		Route::post('/logout', 'Auth\LoginController@logout');
		Route::get('/password/confirm', 'Auth\ConfirmPasswordController@showConfirmForm')->name('password.confirm');
		Route::post('/password/confirm', 'Auth\ConfirmPasswordController@confirm');
		Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
		Route::post('/password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');
		Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
		Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
		Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
		Route::post('/register', 'Auth\RegisterController@register')->name('register');


		Route::group(['middleware' => 'auth'], function () {
		Route::get('/home', 'HomeController@index');
		Route::get('/', 'HomeController@index');
		// 28 feb 2020
		// =========================   CategoryController    ======================================================
		Route::get('/category', 'CategoryController@index')->name('category');
		Route::get('/category/create', 'CategoryController@create')->name('create.category');
		Route::post('/category/store', 'CategoryController@store')->name('store.category');
		Route::get('/category/show/{id}', 'CategoryController@show')->name('show.category');
		Route::post('/category/update/{id}', 'CategoryController@update')->name('update.category');
		Route::get('/category/delete/{id}', 'CategoryController@destroy')->name('delete.category');
		
		// =========================   ProductController    =======================================================
		Route::get('/product', 'ProductController@index')->name('product');
		Route::get('/product/create', 'ProductController@create')->name('create.product');
		Route::post('/product/store', 'ProductController@store')->name('store.product');
		Route::get('/product/show/{id}', 'ProductController@show')->name('show.product');
		Route::post('/product/update/{id}', 'ProductController@update')->name('update.product');
		Route::get('/product/delete/{id}', 'ProductController@destroy')->name('delete.product');
		Route::post('/product/update_toggle', 'ProductController@update_toggle')->name('update_toggle.product');
		Route::post('/product/feature_toggle', 'ProductController@feature_toggle')->name('feature_toggle.product');
		Route::get('/product/delete_product_gallery/{id}', 'ProductController@destroy_product_gallery')->name('delete.product_gallery');
		
		// =========================   UserController    =======================================================
		Route::get('/user', 'UserController@index')->name('user');
		Route::get('/user/create', 'UserController@create')->name('create.user');
		Route::post('/user/store', 'UserController@store')->name('store.user');
		Route::get('/user/show/{id}', 'UserController@show')->name('show.user');
		Route::post('/user/update/{id}', 'UserController@update')->name('update.user');
		Route::get('/user/delete/{id}', 'UserController@destroy')->name('delete.user');
		Route::post('/user/update_toggle', 'UserController@update_toggle')->name('update_toggle.user');
	
		// =========================   SellerController    =======================================================
		Route::get('/seller', 'SellerController@index')->name('seller');
		Route::get('/seller/create', 'SellerController@create')->name('create.seller');
		Route::post('/seller/store', 'SellerController@store')->name('store.seller');
		Route::get('/seller/show/{id}', 'SellerController@show')->name('show.seller');
		Route::post('/seller/update/{id}', 'SellerController@update')->name('update.seller');
		Route::get('/seller/delete/{id}', 'SellerController@destroy')->name('delete.seller');
		Route::post('/seller/update_toggle', 'SellerController@update_toggle')->name('update_toggle.seller');
	
		// =========================   BlogController    =======================================================
		Route::get('/blog', 'BlogController@index')->name('blog');
		Route::get('/blog/create', 'BlogController@create')->name('create.blog');
		Route::post('/blog/store', 'BlogController@store')->name('store.blog');
		Route::get('/blog/show/{id}', 'BlogController@show')->name('show.blog');
		Route::post('/blog/update/{id}', 'BlogController@update')->name('update.blog');
		Route::get('/blog/delete/{id}', 'BlogController@destroy')->name('delete.blog');
		Route::post('/blog/update_toggle', 'BlogController@update_toggle')->name('update_toggle.blog');

		// Route::resource('user', 'UserController', ['except' => ['show']]);
		// Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
		// Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
		// Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
	
		// =========================   Admin ProfileController =======================================================
		Route::get('/profile', 'ProfileController@edit')->name('profile.edit');
		Route::put('/profile/update', 'ProfileController@update')->name('profile.update');
		Route::put('/profile/password', 'ProfileController@password')->name('profile.password');

		// =========================   		EnquiryController    =======================================================
		Route::get('/enquiry', 'EnquiryController@index')->name('enquiry');

		// end 
	});	
});

