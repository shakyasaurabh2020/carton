<?php

use Illuminate\Http\Request;
  header('Access-Control-Allow-Origin: *');
  header("Access-Control-Allow-Headers: Origin, *");
//  header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept,Authorization");
  header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
// Route::group(['middleware' => 'cors'], function () {
//++++++++++++++++++++++++++++++ AuthController  ++++++++++++++++++++++++++++++
Route::post('/registration', 'API\AuthController@registration');
Route::post('/login', 'API\AuthController@login');
Route::get('/country', 'API\AuthController@country');
Route::get('/state', 'API\AuthController@state');
Route::post('/forgot_password', 'API\AuthController@forgot_password');// Create token password reset(forget pwd)
Route::get('/match_token/{token}', 'API\AuthController@match_token'); //  Find token password reset(forget pwd)
Route::post('/reset_password', 'API\AuthController@reset_password');

//++++++++++++++++++++++++++++++ HomeController   ++++++++++++++++++++++++++++++
Route::get('/home', 'API\HomeController@index');
Route::post('/inquiry_form', 'API\HomeController@inquiry');

//++++++++++++++++++++++++++++++ Controller   ++++++++++++++++++++++++++++++
Route::get('/search', 'API\SearchController@index');
Route::get('/category', 'API\CategoryController@index');

//++++++++++++++++++++++++++++++ ProductController++++++++++++++++++++++++++++++
Route::get('/product', 'API\ProductController@index');
Route::get('/product_details', 'API\ProductController@show');

//++++++++++++++++++++++++++++++ BlogController++++++++++++++++++++++++++++++
Route::get('/blog', 'API\BlogController@index');
Route::get('/blog_details', 'API\BlogController@show');

//++++++++++++++++++++++++++++++ profileController++++++++++++++++++++++++++++++
Route::get('/profile_info', 'API\ProfileController@profile_info');
Route::post('/profile_contact', 'API\ProfileController@contact_info');
Route::post('/profile_company', 'API\ProfileController@company_info');
Route::post('/password_change', 'API\ProfileController@password_change');

//++++++++++++++++++++++++++++++ SellerProductController++++++++++++++++++++++++++++++
Route::get('/seller_product_list', 'API\SellerProductController@index');
Route::get('/fetch_seller_product', 'API\SellerProductController@show');
Route::get('/dropdown_product', 'API\SellerProductController@dropdown');
Route::delete('/delete_product', 'API\SellerProductController@delete');
Route::post('/add_product', 'API\SellerProductController@store');
Route::post('/update_product/{id}', 'API\SellerProductController@update');
Route::delete('/delete_product_gallery', 'API\SellerProductController@destroy_product_gallery');
Route::post('/product_feature', 'API\SellerProductController@feature');
Route::post('/product_status', 'API\SellerProductController@status');

//++++++++++++++++++++++++++++++ InquiryController  ++++++++++++++++++++++++++++++
Route::get('/inquiry_list', 'API\InquiryController@index');
Route::get('/view_enquiry', 'API\InquiryController@show');
// view_enquiry
//++++++++++++++++++++++++++++++ SubscribeController  ++++++++++++++++++++++++++++++
Route::post('/subscribe', 'API\SubscribeController@store');

// });
//   Route::group(['middleware' => 'jwt.auth'], function ()  {
//     Route::get('/profile_info', 'API\ProfileController@profile_info');
//   }); 


